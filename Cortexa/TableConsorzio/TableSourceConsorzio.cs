﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace Cortexa
{
    class TableSourceConsorzio : UITableViewSource
    {

        static string TXT_CHISIAMO = "Nel luglio 2007 nasce CORTEXA, il Consorzio per la cultura del Sistema a Cappotto.\n\n\nCORTEXA, il consorzio italiano per la cultura del Sistema a Cappotto, unisce sotto lo stesso marchio le più grandi aziende del settore sfruttando la loro esperienza trentennale.\n\nIl Consorzio si propone di diffondere la cultura dell'isolamento a cappotto, mettendo a disposizione le conoscenze delle aziende associate per assicurare al mercato italiano un alto standard tecnologico finalizzato al conseguimento di obiettivi come risparmio energetico e vantaggi economici, termici, strutturali e di durata nel tempo.\n\nL'esperienza del gruppo di aziende consorziate garantisce controlli completi sui singoli componenti e sull'applicazione dei prodotti, consulenze tecniche qualificate e la continua assistenza in cantiere.\n\nIl Consorzio fornisce inoltre una formazione continua agli specialisti del settore, per diffondere la conoscenza e offrire qualità anche nella fase diagnostica, progettuale ed esecutiva dell'installazione del Sistema a Cappotto.\n\nCortexa è socio fondatore di EAE, l'Associazione Europea per il Sistema di Isolamento a Cappotto.";
        static string TXT_CONSORZIATI = "Tutte le aziende associate a Cortexa vantano più di 30 anni di esperienza nel settore e milioni di metri quadri di facciate di edifici isolati in Italia e nel mondo, perché solo l’esperienza e il know-how di chi è da tempo sul mercato può garantire affidabilità e qualità. \nLe aziende fondatrici, fortemente specializzate nel settore della protezione termica integrale e costantemente impegnate ad investire in formazione e ricerca nel campo dell’isolamento termico in edilizia, migliorano continuamente i propri prodotti e la propria offerta al mercato.\nOffrono un servizio di consulenza e di assistenza specializzata sul cantiere, oltre ad una gamma completa ed organizzata di sistemi professionali per l’isolamento termico.\n\nAderire a Cortexa significa per le aziende consorziate continuare il proprio percorso di miglioramento delle prestazioni e di garanzia della qualità.\n\nSoci Ordinari:\n\n\t●Alligator Italia\n\t●Baumit Italia\n\t●Caparol Italiana\n\t●Ivas - Industria Vernici\n\t●Röfix\n\t●Settef\n\t●Sigma Coatings\n\t●Sto Italia\n\t●Viero\n\t●Waler\n\n\nMain Partner:\n\n\t●BASF\n\t●Saint Gobain - Isover\n\t●Knauf Insulation\n\t●Rockwool\n\t●Stiferite\n\t●Eni Versalis\n\n\nPartner:\n\n\t●EJOT\n\n\nPartner Tecnico:\n\n\t●AIPE\n\t●FIVRA";
        static string TXT_PERCHECONSORZIO = "Nel luglio 2007 nasce CORTEXA, il Consorzio per la cultura del Sistema a Cappotto.\n\n\nCORTEXA, il consorzio italiano per la cultura del Sistema a Cappotto, unisce sotto lo stesso marchio le più grandi aziende del settore sfruttando la loro esperienza trentennale.\n\nIl Consorzio si propone di diffondere la cultura dell'isolamento a cappotto, mettendo a disposizi";

        bool FirstOpen = false;
        bool SecondOpen = false;
        bool ThirdOpen = false;


        UIImage plus = UIImage.FromFile("Plus.png");
        UIImage minus = UIImage.FromFile("Minus.png");

        UIScrollView scrollview;

        CGSize size1, size2, size3;
        int righe;

        bool isPhone;

        float widht;

        ConsorzioPageController super;

        public TableSourceConsorzio(float w, ConsorzioPageController s)
        {
            righe = 6;
            widht = w;
            super = s;

            size1 = UIStringDrawing.StringSize(TXT_CHISIAMO, UIFont.FromName("OpenSans", 14), new CGSize(w, 2000));
            size2 = UIStringDrawing.StringSize(TXT_CONSORZIATI, UIFont.FromName("OpenSans", 14), new CGSize(w , 2000));
            size3 = UIStringDrawing.StringSize(TXT_PERCHECONSORZIO, UIFont.FromName("OpenSans", 14), new CGSize(w , 2000));

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return righe;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {

            if (indexPath.Row == 0)
            {
                if (FirstOpen)
                {
                    FirstOpen = false;
                    super.CloseChange((float)size1.Height);
                }
                else
                {
                    FirstOpen = true;
                    super.OpenChange((float)size1.Height);

                    if (SecondOpen)
                    {
                        SecondOpen = false;
                        super.CloseChange((float)size2.Height);
                    }
                    if (ThirdOpen)
                    {
                        ThirdOpen = false;
                        super.CloseChange((float)size3.Height);
                    }
                }
            }
            if (indexPath.Row == 2)
            {
                if (SecondOpen)
                {
                    SecondOpen = false;
                    super.CloseChange((float)size2.Height);
                }
                else
                {
                    SecondOpen = true;
                    super.OpenChange((float)size2.Height);

                    if (FirstOpen)
                    {
                        FirstOpen = false;
                        super.CloseChange((float)size1.Height);
                    }
                    if (ThirdOpen)
                    {
                        ThirdOpen = false;
                        super.CloseChange((float)size3.Height);
                    }
                }
            }
            if (indexPath.Row == 4)
            {
                if (ThirdOpen)
                {
                    ThirdOpen = false;
                    super.CloseChange((float)size3.Height);
                }
                else
                {
                    ThirdOpen = true;
                    super.OpenChange((float)size3.Height);

                    if (FirstOpen)
                    {
                        FirstOpen = false;
                        super.CloseChange((float)size1.Height);
                    }
                    if (SecondOpen)
                    {
                        SecondOpen = false;
                        super.CloseChange((float)size2.Height);
                    }
                }
            }

            tableView.ReloadData();
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;
            if (row == 1 && FirstOpen)
            {
                return (nfloat)(size1.Height + 15);
            }
            if (row == 1 && !FirstOpen)
            {
                return 15;
            }
            if (row == 3 && SecondOpen)
            {
                return (nfloat)(size2.Height + 15);
            }
            if (row == 3 && !SecondOpen)
            {
                return 15;
            }
            if (row == 5 && ThirdOpen)
            {
                return (nfloat)(size3.Height + 15);
            }
            if (row == 5 && !ThirdOpen)
            {
                return 15;
            }

            float NormalHeight = 37;

            return (nfloat)NormalHeight;

        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            int row = indexPath.Row;

            if (row == 0 && FirstOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("Chi Siamo" , minus);
                return cell;
            }
            if (row == 0 && !FirstOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("Chi Siamo", plus);
                return cell;
            }

            if (row == 2 && SecondOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("I consorziati", minus);
                return cell;
            }
            if (row == 2 && !SecondOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("I consorziati", plus);
                return cell;
            }

            if (row == 4 && ThirdOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("Perchè il consorzio", minus);
                return cell;
            }
            if (row == 4 && !ThirdOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioTitolo cell = new CustomCellConsorzioTitolo(cellIdentifier);
                cell.UpdateCell("Perchè il consorzio", plus);
                return cell;
            }
            

            if (row == 1 && FirstOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, (float)size1.Height);
                cell.UpdateCell(TXT_CHISIAMO);
                return cell;
            }
            if (row == 1 && !FirstOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, 0);
                return cell;
            }

            if (row == 3 && SecondOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, (float)size2.Height);
                cell.UpdateCell(TXT_CONSORZIATI);
                return cell;
            }
            if (row == 3 && !SecondOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, 0);
                return cell;
            }

            if (row == 5 && ThirdOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, (float)size3.Height);
                cell.UpdateCell(TXT_PERCHECONSORZIO);
                return cell;
            }
            if (row == 5 && !ThirdOpen)
            {
                NSString cellIdentifier = new NSString(row.ToString());
                CustomCellConsorzioDescrizione cell = new CustomCellConsorzioDescrizione(cellIdentifier, 0);
                return cell;
            }

            return null;

        }
    }
}