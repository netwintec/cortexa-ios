using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace Cortexa
{
    class CustomCellConsorzioTitolo : UITableViewCell
    {
        UILabel titolo;
        UIView separator;
        UIImageView image;

        public CustomCellConsorzioTitolo(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
        {


            SelectionStyle = UITableViewCellSelectionStyle.None;
            image = new UIImageView();
            separator = new UIView();
            separator.BackgroundColor = UIColor.Black;

            titolo = new UILabel()
            {
                Font = UIFont.FromName("Opensans", 17f),
                TextColor = UIColor.FromRGB(39,78,139),
                //Font = UIFont.BoldSystemFontOfSize(16),
                //Lines = 2,
                BackgroundColor = UIColor.Clear
            };

            ContentView.BackgroundColor = UIColor.White;

            ContentView.AddSubviews(new UIView[] { titolo, image, separator });

        }
        public void UpdateCell(string tit, UIImage imm)
        {
            titolo.Text = tit;
            image.Image = imm;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            titolo.Frame = new CGRect(0, 8.5, ContentView.Frame.Width - 40, 20);
            image.Frame = new CGRect(ContentView.Frame.Width - 30, 3.5, 30, 30);
            separator.Frame = new CGRect(0, 35, ContentView.Frame.Width, 1);
            

        }
    }

}
