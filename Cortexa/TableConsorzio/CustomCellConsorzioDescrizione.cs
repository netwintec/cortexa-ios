using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using CoreGraphics;

namespace Cortexa
{
    class CustomCellConsorzioDescrizione : UITableViewCell
    {
        UILabel titolo;

        float Height;
        public CustomCellConsorzioDescrizione(NSString cellId,float h) : base(UITableViewCellStyle.Default, cellId)
        {

            Height = h;

            SelectionStyle = UITableViewCellSelectionStyle.None;

            titolo = new UILabel()
            {
                Font = UIFont.FromName("Opensans", 14f),
                TextColor = UIColor.Black,
                Lines = 0,
                BackgroundColor = UIColor.Clear
            };

            ContentView.BackgroundColor = UIColor.White;

            ContentView.AddSubviews(new UIView[] { titolo});

        }
        public void UpdateCell(string tit)
        {
            titolo.Text = tit;
        }
        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            titolo.Frame = new CGRect(0, 5, ContentView.Frame.Width , Height);

        }
    }
}
