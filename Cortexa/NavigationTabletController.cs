using Foundation;
using System;
using UIKit;

namespace Cortexa
{
    public partial class NavigationTabletController : UINavigationController
    {
        public NavigationTabletController (IntPtr handle) : base (handle)
        {
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return VisibleViewController.GetSupportedInterfaceOrientations();
        }

        public override bool ShouldAutorotate()
        {
            return VisibleViewController.ShouldAutorotate();
        }

    }
}