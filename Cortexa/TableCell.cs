using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Cortexa
{
    public partial class TableCell : UITableViewCell
    {
        public TableCell (IntPtr handle) : base (handle)
        {
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            this.BackgroundColor = UIColor.FromRGB(78, 78, 78);

            if (this.ReuseIdentifier == new NSString("one"))
            {
                Label0.Font = UIFont.FromName("OpenSans", 17);
                Separator0.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("two"))
            {
                Label1.Font = UIFont.FromName("OpenSans", 17);
                Separator1.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("three")) {
                Label2.Font = UIFont.FromName("OpenSans", 17);
                Separator2.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("four"))
            {
                Label3.Font = UIFont.FromName("OpenSans", 17);
                Separator3.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("five"))
            {
                Label4.Font = UIFont.FromName("OpenSans", 17);
                Separator4.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("six"))
            {
                Label5.Font = UIFont.FromName("OpenSans", 17);
                Separator5.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("seven"))
            {
                Label6.Font = UIFont.FromName("OpenSans", 17);
                Separator6.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("eight"))
            {
                Label7.Font = UIFont.FromName("OpenSans", 17);
                Separator7.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }
            if (this.ReuseIdentifier == new NSString("nine"))
            {
                Label8.Frame = new CGRect(15, 20, AppDelegate.Instance.RevealWidht - 30, 20);
                Label8.Font = UIFont.FromName("OpenSans", 15);
                Separator8.Frame = new CGRect(15, 57, AppDelegate.Instance.RevealWidht - 30, 1);
            }

        }
    
    }
}