// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Cortexa
{
    [Register ("SondaggioHomePageController")]
    partial class SondaggioHomePageController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView Load { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem MenuItem { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BackIcon != null) {
                BackIcon.Dispose ();
                BackIcon = null;
            }

            if (ContentView != null) {
                ContentView.Dispose ();
                ContentView = null;
            }

            if (Load != null) {
                Load.Dispose ();
                Load = null;
            }

            if (MenuItem != null) {
                MenuItem.Dispose ();
                MenuItem = null;
            }
        }
    }
}