using CoreGraphics;
using Foundation;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class SondaggioHomePageController : UIViewController
    {

        UILabel noSon;

        float W, H, WApp, HApp;
        public bool change = false;

        public static SondaggioHomePageController Instance { private set; get; }

        public SondaggioHomePageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            SondaggioHomePageController.Instance = this;

            int PageGiusta = 11;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            
            var size = UIStringDrawing.StringSize("Nessun sondaggio disponibile al momento", UIFont.FromName("OpenSans", 17), new CGSize(ContentView.Frame.Width - 20, 2000));

            noSon = new UILabel(new CGRect(ContentView.Frame.Width / 2 - size.Width / 2, ContentView.Frame.Height/2 - size.Height/2,size.Width,size.Height));
            noSon.Text = "Nessun sondaggio disponibile al momento";
            noSon.Font = UIFont.FromName("OpenSans", 17);
            noSon.TextColor = UIColor.Black;
            noSon.Lines = 0;
            noSon.TextAlignment = UITextAlignment.Center;
            noSon.Alpha = 0;

            ContentView.Add(noSon);
            
            Load.StartAnimating();
            LoadSondaggi();

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Home");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }


        public void LoadSondaggi()
        {

            //***** DOWNLOAD SONDAGGIO ****
            var clientSondaggio = new RestClient("http://www.cortexa.it/");

            var requestSondaggio = new RestRequest("cortexa-app/sondaggi.php?token=" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa"), Method.GET);


            clientSondaggio.ExecuteAsync(requestSondaggio, (s, e) =>
            {

                Console.WriteLine("Result Sondaggio:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListSondaggio = JsonConvert.DeserializeObject<List<SondaggioObject>>(s.Content);

                    InvokeOnMainThread(() => {

                        CreateView();

                    });

                }

                else
                {
                    InvokeOnMainThread(() =>
                    {
                        Load.Alpha = 0;
                        noSon.Alpha = 1;
                    });
                }
            });

        }


        public void CreateView()
        {
            /*string tit = "TITOLO";

            var size2 = UIStringDrawing.StringSize(tit, UIFont.FromName("OpenSans", 17), new CGSize(ContentView.Frame.Width - 20, 2000));

            UILabel Title = new UILabel(new CGRect(ContentView.Frame.Width / 2 - size2.Width / 2, 0, size2.Width, size2.Height));
            Title.Text = tit;
            Title.Font = UIFont.FromName("OpenSans", 17);
            Title.TextColor = UIColor.Black;
            Title.Lines = 0;*/

            UIView Contorno = new UIView(new CGRect(ContentView.Frame.Width / 2 - 75, 0, 150, 50));
            Contorno.BackgroundColor = UIColor.FromRGB(200, 200, 200);
            Contorno.Layer.CornerRadius = 5;

            UIView Interno = new UIView(new CGRect(2, 2, 146, 46));
            Interno.BackgroundColor = UIColor.FromRGB(138, 138, 138);
            Interno.Layer.CornerRadius = 4;

            UIButton btn = new UIButton(new CGRect(2, 2, 142, 42));
            btn.SetTitle("Inizia Sondaggio", UIControlState.Normal);
            btn.SetTitleColor(UIColor.White, UIControlState.Normal);
            btn.Font = UIFont.FromName("OpenSans", 16);
            btn.TouchUpInside += delegate {

                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("Main_iPad", null);
                }
                UIViewController SP = storyboard.InstantiateViewController("Sondaggio");
                this.RevealViewController().PushFrontViewController(SP, true);

            };

            Interno.Add(btn);
            Contorno.Add(Interno);

            float h = (float)50;

            UIView Start = new UIView(new CGRect(0, ContentView.Frame.Height / 2 - h / 2, ContentView.Frame.Width, h));

            Start.Add(Contorno);

            ContentView.Add(Start);

            Load.Alpha = 0;
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }

    }
}