using CoreGraphics;
using Foundation;
using System;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class NewsDettaglioPageController : UIViewController
    {

        NewsObject no;

        UIView TextView;
        UIScrollView scrollview;

        int yy = 0;

        float W, H, WApp, HApp;
        public bool change = false;

        public static NewsDettaglioPageController Instance { private set; get; }

        public NewsDettaglioPageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NewsDettaglioPageController.Instance = this;

            int PageGiusta = 4;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            no = NewsPageController.Instance.NewsSelected;

            scrollview = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));


            UIImageView image = new UIImageView(new CGRect(ContentView.Frame.Width/2 -50, 5, 100, 100));
            image.Image = UIImage.FromFile("placeholder.png");

            if (no.Image == null)
                SetImageAsync(image, no, no.immagine);
            else
            {

                nfloat w = no.Image.Size.Width;
                nfloat h = no.Image.Size.Height;

                float newWidht, newHeight;

                newWidht = (float)ContentView.Frame.Width-40;
                newHeight = (float)((newWidht * h) / w);

                if (newHeight > 175)
                {

                    newHeight = (float)175;
                    newWidht = (float)((175 * w) / h);

                }

                Console.WriteLine(w + "  " + h);

                image.Frame = new CGRect(ContentView.Frame.Width / 2 - (newWidht / 2), 5, newWidht, newHeight);


                image.Image = no.Image;
            }

            yy += (int)image.Frame.Height + 5;


            int yyy = 0;

            var size1 = UIStringDrawing.StringSize(no.titolo, UIFont.FromName("OpenSans", 17), new CGSize(ContentView.Frame.Width - 40, 2000));

            UILabel tit = new UILabel(new CGRect(20, yyy + 10, ContentView.Frame.Width-40, size1.Height));
            tit.Text = no.titolo;
            tit.TextColor = UIColor.FromRGB(134, 58, 128);
            tit.Font = UIFont.FromName("OpenSans", 17);
            tit.Lines = 0;

            yyy += (int)size1.Height + 5;

            UILabel data = new UILabel(new CGRect(20, yyy+3, ContentView.Frame.Width - 40, 17));
            data.Text = no.data;
            data.TextColor = UIColor.Black;
            data.Font = UIFont.FromName("OpenSans-Bold", 13);

            yyy += 20;

            size1 = UIStringDrawing.StringSize(no.testo, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width - 40, 2000));

            UILabel test = test = new UILabel(new CGRect(20, yyy + 10, ContentView.Frame.Width - 40, size1.Height));
            test.Text = no.testo;
            test.TextColor = UIColor.Black;
            test.Font = UIFont.FromName("OpenSans", 14);
            test.Lines = 0;

            yyy += (int)size1.Height + 10;

            TextView = new UIView(new CGRect(0, yy, ContentView.Frame.Width, yyy));

            TextView.Add(tit);
            TextView.Add(data);
            TextView.Add(test);

            yy += yyy;

            scrollview.Add(image);
            scrollview.Add(TextView);

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy + 20);

            ContentView.Add(scrollview);


            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("News");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public void SetImageAsync(UIImageView image, NewsObject obj, string url)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float newWidht, newHeight;

                        newWidht = (float)ContentView.Frame.Width - 40;
                        newHeight = (float)((newWidht * h) / w);

                        if (newHeight > 175)
                        {

                            newHeight = (float)175;
                            newWidht = (float)((175 * w) / h);

                        }

                        Console.WriteLine(w + "  " + h);

                        image.Frame = new CGRect(ContentView.Frame.Width / 2 - (newWidht / 2), 5, newWidht, newHeight);

                        image.Image = cachedImage;
                        obj.Image = cachedImage;

                        CGRect frame2 = TextView.Frame;
                        frame2.Y += (newHeight - 100);
                        TextView.Frame = frame2;

                        scrollview.ContentSize = new CGSize(View.Frame.Width, yy + (newHeight - 100));
                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
            {
                b.RemoveFromSuperview();
            }

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }
    }
}