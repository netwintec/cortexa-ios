using Foundation;
using RestSharp;
using System;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class MenuController : UITableViewController
    {
        public int selected;

        UIStoryboard storyboard;

        UIViewController HP, CP, NP, VP, WP, PP, MP, SHP;

        public MenuController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var tableView = View as UITableView;

            if (tableView != null)
            {
                tableView.Source = new TableSource(this);
                tableView.ReloadData();
            }

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            HP = storyboard.InstantiateViewController("Home");
            CP = storyboard.InstantiateViewController("Consorzio");
            NP = storyboard.InstantiateViewController("News");
            VP = storyboard.InstantiateViewController("Video");
            WP = storyboard.InstantiateViewController("Webinar");
            PP = storyboard.InstantiateViewController("Partner");
            MP = storyboard.InstantiateViewController("Manuale");
            SHP = storyboard.InstantiateViewController("SondaggioHome");

        }

        public void ChangePage(int i) {

            if(i == 0)
                this.RevealViewController().PushFrontViewController(HP, true);
            if (i == 1)
                this.RevealViewController().PushFrontViewController(CP, true);
            if (i == 2)
                this.RevealViewController().PushFrontViewController(NP, true);
            if (i == 3)
                this.RevealViewController().PushFrontViewController(VP, true);
            if (i == 4)
                this.RevealViewController().PushFrontViewController(WP, true);
            if (i == 5)
                this.RevealViewController().PushFrontViewController(PP, true);
            if (i == 6)
                this.RevealViewController().PushFrontViewController(MP, true);
            if (i == 7)
                this.RevealViewController().PushFrontViewController(SHP, true);
            if (i == 8)
                Logout();

        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Logout()
        {
            UIStoryboard storyboard;
            int button = await ShowAlert("Logout", "Vuoi effettuare il Logout", "Ok", "Annulla");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                //******** DELETE TOKEN PUSH *******
                try
                {

                    var client = new RestClient("http://www.cortexa.it/");

                    var request = new RestRequest("cortexa-app/logout.php?token=" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortex"), Method.GET);

                    client.ExecuteAsync(request, (s, e) =>
                    {

                        Console.WriteLine("Logout" + s.Content + "!" + s.StatusCode);

                    });

                }
                catch (Exception e)
                {
                    Console.WriteLine("Aaaa");
                }
                //************************

                UIStoryboard storyboard2 = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard2 = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                }

                NSUserDefaults.StandardUserDefaults.SetString("", "TokenCortexa");
                UIViewController lp = storyboard2.InstantiateViewController("Login");
                this.PresentModalViewController(lp, true);
            }
            else { }
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }

            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }
    }
}