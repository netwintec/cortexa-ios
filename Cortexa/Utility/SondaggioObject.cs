using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace Cortexa
{
    public class SondaggioObject
    {

        public string sppolls_poll_id { get; set; }
        public string title { get; set; }
        public List<RisposteObject> polls { get; set; }

    }

    public class RisposteObject
    {

        public string poll { get; set; }
        public string votes { get; set; }

    }

    public class AssociazioneObject
    {

        public string poll { get; set; }
        public string sppolls_poll_id { get; set; }

    }
}