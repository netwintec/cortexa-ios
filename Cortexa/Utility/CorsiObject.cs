﻿using System;


namespace Cortexa
{
	public class CorsiObject
	{
		public string id { get; set; }
		public string url_image { get; set; }
		public string abilitato { get; set; }

		public CorsiDettagliObject Dettagli { get; set; }

		public UIKit.UIImage Image;

	}
}

