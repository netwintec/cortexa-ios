﻿using System;

namespace Cortexa
{
	public class BannerObject
	{
		public string id { get; set; }
		public string url { get; set; }

		public UIKit.UIImage image;
	}
}

