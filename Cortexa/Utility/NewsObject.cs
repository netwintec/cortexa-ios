﻿using System;


namespace Cortexa
{
	public class NewsObject
	{
		public string id { get; set; }
		public string titolo { get; set; }
		public string data { get; set; }
		public string testo { get; set; }
		public string immagine { get; set; }

		public UIKit.UIImage Image;
	}
}

