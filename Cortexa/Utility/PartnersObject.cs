﻿using System;


namespace Cortexa
{
	public class PartnersObject
	{
		public string id { get; set; }
		public string nome { get; set; }
		public string immagine { get; set; }
		public string descrizione { get; set; }
		public string sito_web { get; set; }

		public UIKit.UIImage Image;
	}
}

