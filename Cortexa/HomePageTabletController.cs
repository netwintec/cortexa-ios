using CoreGraphics;
using Foundation;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class HomePageTabletController : UIViewController
    {

        static string CONSORZIO = "CONSORZIO";
        static string NEWS = "NEWS";
        static string VIDEO = "VIDEO";
        static string WEBINAR = "WEBINAR";
        static string PARTNERS = "PARTNERS";
        static string MANUALE = "MANUALE";

        double Widht, Height, PaddingX, PaddingY;

        public float RevealWidht;

        List<BannerObject> ListBanner;
        Timer timer;

        float W, H, WApp, HApp;
        public bool change = false;

        bool isPortrait = false;

        int indiceFoto =0;

        public static HomePageTabletController Instance { private set; get; }

        public HomePageTabletController (IntPtr handle) : base (handle)
        {
            
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            HomePageTabletController.Instance = this;

            int PageGiusta = 1;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {

                if(orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;

                    isPortrait = true;

                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;

                    isPortrait = false;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                    isPortrait = true;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                    isPortrait = false;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            ListBanner = AppDelegate.Instance.ListBanner;

            UIView ButtonView;

            if (orientation == UIDeviceOrientation.Portrait)
            {
                ButtonView = new UIView(new CGRect(0,ContentView.Frame.Height / 2,ContentView.Frame.Width,ContentView.Frame.Height/2));
            }
            else {
                ButtonView = new UIView(new CGRect(0,0,ContentView.Frame.Width,ContentView.Frame.Height));
            }

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController CP = storyboard.InstantiateViewController("Consorzio");
            UIViewController NP = storyboard.InstantiateViewController("News");
            UIViewController VP = storyboard.InstantiateViewController("Video");
            UIViewController WP = storyboard.InstantiateViewController("Webinar");
            UIViewController PP = storyboard.InstantiateViewController("Partner");
            UIViewController MP = storyboard.InstantiateViewController("Manuale");

            var size = UIStringDrawing.StringSize(CONSORZIO, UIFont.FromName("OpenSans-Bold", 15), new CGSize(ContentView.Frame.Width / 2, 2000));

            Height = (ButtonView.Frame.Height -(size.Height *2)- 45) /2;
            Widht = Height;

            PaddingX = (ButtonView.Frame.Width - ((Widht) * 3)) / 4;
            PaddingY = 15;

            Console.WriteLine("******** VALORI ********");
            Console.WriteLine("ContentView:" + ContentView.Frame);
            Console.WriteLine("ButtonView:" + ButtonView.Frame);
            Console.WriteLine("Calcolati:"+W + "|" + H);
            Console.WriteLine("W:" + Widht + "|H:" + Height + "|Px:" + PaddingX + "|Py:" + PaddingY);
            Console.WriteLine("sizeText:" + size.Height);
            Console.WriteLine("************************");

            UIButton ConsorzioButton = new UIButton(new CGRect(PaddingX, PaddingY, Widht, Height));
            UIImageView ConsorzioImage = new UIImageView(ConsorzioButton.Bounds);
            ConsorzioImage.Image = UIImage.FromFile("Consorzio.png");
            ConsorzioButton.AddSubview(ConsorzioImage);
            ConsorzioButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(CP, true);
            };

            UILabel ConsorzioLabel = new UILabel(new CGRect(PaddingX, (Height) + (PaddingY), Widht, size.Height));
            ConsorzioLabel.Text = CONSORZIO;
            ConsorzioLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            ConsorzioLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            ConsorzioLabel.TextAlignment = UITextAlignment.Center;


            UIButton NewsButton = new UIButton(new CGRect((PaddingX * 2) + Widht, PaddingY, Widht, Height));
            UIImageView NewsImage = new UIImageView(NewsButton.Bounds);
            NewsImage.Image = UIImage.FromFile("News.png");
            NewsButton.AddSubview(NewsImage);
            NewsButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(NP, true);
            };

            UILabel NewsLabel = new UILabel(new CGRect((PaddingX * 2) + Widht, (Height) + (PaddingY), Widht, size.Height));
            NewsLabel.Text = NEWS;
            NewsLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            NewsLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            NewsLabel.TextAlignment = UITextAlignment.Center;


            UIButton VideoButton = new UIButton(new CGRect((PaddingX * 3) + (Widht*2), PaddingY, Widht, Height));
            UIImageView VideoImage = new UIImageView(VideoButton.Bounds);
            VideoImage.Image = UIImage.FromFile("Video.png");
            VideoButton.AddSubview(VideoImage);
            VideoButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(VP, true);
            };

            UILabel VideoLabel = new UILabel(new CGRect((PaddingX * 3) + (Widht * 2),(Height) + (PaddingY), Widht, size.Height));
            VideoLabel.Text = VIDEO;
            VideoLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            VideoLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            VideoLabel.TextAlignment = UITextAlignment.Center;


            UIButton WebinarButton = new UIButton(new CGRect(PaddingX, (PaddingY * 2) + (Height) + (size.Height), Widht, Height));
            UIImageView WebinarImage = new UIImageView(WebinarButton.Bounds);
            WebinarImage.Image = UIImage.FromFile("Webinar.png");
            WebinarButton.AddSubview(WebinarImage);
            WebinarButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(WP, true);
            };

            UILabel WebinarLabel = new UILabel(new CGRect(PaddingX, (Height * 2) + (PaddingY * 2) + (size.Height), Widht, size.Height));
            WebinarLabel.Text = WEBINAR;
            WebinarLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            WebinarLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            WebinarLabel.TextAlignment = UITextAlignment.Center;


            UIButton PartnerButton = new UIButton(new CGRect(PaddingX*2 + Widht, (Height) + (PaddingY * 2) + (size.Height), Widht, Height));
            UIImageView PartnerImage = new UIImageView(PartnerButton.Bounds);
            PartnerImage.Image = UIImage.FromFile("Partner.png");
            PartnerButton.AddSubview(PartnerImage);
            PartnerButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(PP, true);
            };

            UILabel PartnerLabel = new UILabel(new CGRect(PaddingX * 2 + Widht, (Height * 2) + (PaddingY * 2) + (size.Height), Widht, size.Height));
            PartnerLabel.Text = PARTNERS;
            PartnerLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            PartnerLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            PartnerLabel.TextAlignment = UITextAlignment.Center;


            UIButton ManualeButton = new UIButton(new CGRect((PaddingX * 3)+(Widht * 2), (Height) + (PaddingY * 2) + (size.Height), Widht, Height));
            UIImageView ManualeImage = new UIImageView(ManualeButton.Bounds);
            ManualeImage.Image = UIImage.FromFile("Documenti.png");
            ManualeButton.AddSubview(ManualeImage);
            ManualeButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(MP, true);
            };

            UILabel ManualeLabel = new UILabel(new CGRect((PaddingX * 3) + (Widht * 2), (Height * 2) + (PaddingY * 2) + (size.Height), Widht, size.Height));
            ManualeLabel.Text = MANUALE;
            ManualeLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            ManualeLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            ManualeLabel.TextAlignment = UITextAlignment.Center;


            ButtonView.Add(ConsorzioButton);
            ButtonView.Add(ConsorzioLabel);
            ButtonView.Add(NewsButton);
            ButtonView.Add(NewsLabel);
            ButtonView.Add(VideoButton);
            ButtonView.Add(VideoLabel);
            ButtonView.Add(WebinarButton);
            ButtonView.Add(WebinarLabel);
            ButtonView.Add(PartnerButton);
            ButtonView.Add(PartnerLabel);
            ButtonView.Add(ManualeButton);
            ButtonView.Add(ManualeLabel);

            UIView BannerView = new UIView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height / 2));
            UIImageView image = new UIImageView(new CGRect(BannerView.Frame.Width/2- ((BannerView.Frame.Height - 10)/2), 5, BannerView.Frame.Height - 10, BannerView.Frame.Height -10));
            image.Image = ListBanner[indiceFoto].image;
            
            if (isPortrait)
            {
                BannerView.Add(image);
                ContentView.Add(BannerView);
            }

            timer = new Timer();
            timer.Interval = 3000;
            timer.Elapsed += (sender, e) => {

                InvokeOnMainThread(() => {
                    indiceFoto++;

                    if (indiceFoto == ListBanner.Count)
                        indiceFoto = 0;

                    image.Image = ListBanner[indiceFoto].image;
                });

            };
            timer.Enabled = true;

            if (isPortrait)
                timer.Start();

            ContentView.Add(ButtonView);

            bool push = NSUserDefaults.StandardUserDefaults.BoolForKey("isCortexaNotification");
            Console.WriteLine("PUSH:" + push);
            if (push)
            {
                NSUserDefaults.StandardUserDefaults.SetBool(false, "isCortexaNotification");

                string type = NSUserDefaults.StandardUserDefaults.StringForKey("TypeCortexaNotification");
                UIViewController SHP = storyboard.InstantiateViewController("SondaggioHome");

                if (type == "sondaggio")
                    this.RevealViewController().PushFrontViewController(SHP, true);
                if (type == "news")
                    this.RevealViewController().PushFrontViewController(NP, true);
                if (type == "manuale")
                    this.RevealViewController().PushFrontViewController(MP, true);
                if (type == "corso")
                    this.RevealViewController().PushFrontViewController(WP, true);
                if (type == "video")
                    this.RevealViewController().PushFrontViewController(VP, true);


            }


            if (!AppDelegate.Instance.PushSend)
            {
                if (!string.IsNullOrWhiteSpace(AppDelegate.Instance.APNToken))
                {

                    string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa");

                    var client = new RestClient("http://www.cortexa.it/");

                    var request = new RestRequest("cortexa-app/tokenpush.php?token=" + token + "&device=ios&push=" + AppDelegate.Instance.APNToken, Method.GET);


                    client.ExecuteAsync(request, (s, e) =>
                    {
                    });
                }

                AppDelegate.Instance.PushSend = true;
            }

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            AppDelegate.Instance.RevealWidht = (float)this.RevealViewController().RightViewRevealWidth;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public override void ViewDidDisappear(bool animated)
        {
            timer.Stop();
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;

            timer.Stop();

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }
    }
}