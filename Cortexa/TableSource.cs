using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    class TableSource : UITableViewSource
    {

        public MenuController super;

        public TableSource(MenuController s)
        {
            super = s;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return 9;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            super.ChangePage(indexPath.Row);
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {


            string cellIdentifier = @"Cell";

            switch (indexPath.Row)
            {
                case 0:
                    cellIdentifier = @"one";
                    break;

                case 1:
                    cellIdentifier = @"two";
                    break;

                case 2:
                    cellIdentifier = @"three";
                    break;

                case 3:
                    cellIdentifier = @"four";
                    break;

                case 4:
                    cellIdentifier = @"five";
                    break;

                case 5:
                    cellIdentifier = @"six";
                    break;

                case 6:
                    cellIdentifier = @"seven";
                    break;

                case 7:
                    cellIdentifier = @"eight";
                    break;

                case 8:
                    cellIdentifier = @"nine";
                    break;
            }

            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);
            return cell;

            //throw new NotImplementedException();
        }
    }
}