using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class PartnersPageController : UIViewController
    {

        List<PartnersObject> ListPartner;
        List<UIImageView> listImage = new List<UIImageView>();

        public PartnersObject PartnerSelected;

        float W, H, WApp, HApp;
        public bool change = false;


        public static PartnersPageController Instance { private set; get;}

        public PartnersPageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            int PageGiusta = 8;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            PartnersPageController.Instance = this;

            ListPartner = AppDelegate.Instance.ListPartners;
            listImage.Clear();

            UIScrollView scrollview = new UIScrollView(new CGRect(0,0,ContentView.Frame.Width,ContentView.Frame.Height));

            int yy = 0;

            foreach (PartnersObject po in ListPartner) {

                UIView container = new UIView(new CGRect(ContentView.Frame.Width/2 - 102, yy + 10, 204, 104));
                container.BackgroundColor = UIColor.FromRGB(164,35,40);

                UIView interno = new UIView(new CGRect(2,2,200,100));
                interno.BackgroundColor = UIColor.White;

                UIImageView image = new UIImageView(new CGRect(50, 0, 100, 100));
                image.Image = UIImage.FromFile("placeholder.png");

                if (po.Image == null)
                    SetImageAsync(image, po, po.immagine);
                else
                {

                    nfloat w = po.Image.Size.Width;
                    nfloat h = po.Image.Size.Height;

                    float newWidht, newHeight;

                    newWidht = (float)200;
                    newHeight = (float)((200 * h) / w);

                    if (newHeight > 100)
                    {

                        newHeight = (float)100;
                        newWidht = (float)((100 * w) / h);

                    }

                    Console.WriteLine(w + "  " + h);

                    image.Frame = new CGRect(100 - (newWidht / 2), 50 - (newHeight / 2), newWidht, newHeight);


                    image.Image = po.Image;
                }


                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listImage.Count; ii++)
                    {
                        Console.WriteLine((listImage[ii] == s.View) + "|" + ii);
                        if (listImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    PartnerSelected = ListPartner[giusto];

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }
                    UIViewController PDP = storyboard.InstantiateViewController("PartnerDettaglio");
                    this.RevealViewController().PushFrontViewController(PDP, true);

                });
                image.UserInteractionEnabled = true;
                image.AddGestureRecognizer(imgTap);

                listImage.Add(image);

                interno.Add(image);
                container.Add(interno);
                scrollview.Add(container);
                yy += 114;


            }

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            ContentView.Add(scrollview);

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Home");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }


        public void SetImageAsync(UIImageView image,PartnersObject obj, string url)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float newWidht, newHeight;

                        newWidht = (float)200;
                        newHeight = (float)((200 * h) / w);

                        if(newHeight > 100) {

                            newHeight = (float)100;
                            newWidht = (float)((100 * w) / h);

                        }

                        Console.WriteLine(w + "  " + h);
                        
                        image.Frame = new CGRect(100-(newWidht/2), 50- (newHeight / 2), newWidht, newHeight);

                        image.Image = cachedImage;
                        obj.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }

    }
}