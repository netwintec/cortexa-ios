// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Cortexa
{
    [Register ("WebinarDettaglioPageController")]
    partial class WebinarDettaglioPageController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem BackIcon { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ChiudiButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ChiudiView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem MenuItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView RequisitiView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView TextView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BackIcon != null) {
                BackIcon.Dispose ();
                BackIcon = null;
            }

            if (ChiudiButton != null) {
                ChiudiButton.Dispose ();
                ChiudiButton = null;
            }

            if (ChiudiView != null) {
                ChiudiView.Dispose ();
                ChiudiView = null;
            }

            if (ContentView != null) {
                ContentView.Dispose ();
                ContentView = null;
            }

            if (MenuItem != null) {
                MenuItem.Dispose ();
                MenuItem = null;
            }

            if (RequisitiView != null) {
                RequisitiView.Dispose ();
                RequisitiView = null;
            }

            if (TextView != null) {
                TextView.Dispose ();
                TextView = null;
            }
        }
    }
}