﻿using CoreGraphics;
using Foundation;
using System;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class ConsorzioPageController : UIViewController
    {

        static string TXT_CHISIAMO = "Nel luglio 2007 nasce CORTEXA, il Consorzio per la cultura del Sistema a Cappotto.\n\n\nCORTEXA, il consorzio italiano per la cultura del Sistema a Cappotto, unisce sotto lo stesso marchio le più grandi aziende del settore sfruttando la loro esperienza trentennale.\n\nIl Consorzio si propone di diffondere la cultura dell'isolamento a cappotto, mettendo a disposizione le conoscenze delle aziende associate per assicurare al mercato italiano un alto standard tecnologico finalizzato al conseguimento di obiettivi come risparmio energetico e vantaggi economici, termici, strutturali e di durata nel tempo.\n\nL'esperienza del gruppo di aziende consorziate garantisce controlli completi sui singoli componenti e sull'applicazione dei prodotti, consulenze tecniche qualificate e la continua assistenza in cantiere.\n\nIl Consorzio fornisce inoltre una formazione continua agli specialisti del settore, per diffondere la conoscenza e offrire qualità anche nella fase diagnostica, progettuale ed esecutiva dell'installazione del Sistema a Cappotto.\n\nCortexa è socio fondatore di EAE, l'Associazione Europea per il Sistema di Isolamento a Cappotto.";
        static string TXT_CONSORZIATI = "Tutte le aziende associate a Cortexa vantano più di 30 anni di esperienza nel settore e milioni di metri quadri di facciate di edifici isolati in Italia e nel mondo, perché solo l’esperienza e il know-how di chi è da tempo sul mercato può garantire affidabilità e qualità. \nLe aziende fondatrici, fortemente specializzate nel settore della protezione termica integrale e costantemente impegnate ad investire in formazione e ricerca nel campo dell’isolamento termico in edilizia, migliorano continuamente i propri prodotti e la propria offerta al mercato.\nOffrono un servizio di consulenza e di assistenza specializzata sul cantiere, oltre ad una gamma completa ed organizzata di sistemi professionali per l’isolamento termico.\n\nAderire a Cortexa significa per le aziende consorziate continuare il proprio percorso di miglioramento delle prestazioni e di garanzia della qualità.\n\nSoci Ordinari:\n\n\t●Alligator Italia\n\t●Baumit Italia\n\t●Caparol Italiana\n\t●Ivas - Industria Vernici\n\t●Röfix\n\t●Settef\n\t●Sigma Coatings\n\t●Sto Italia\n\t●Viero\n\t●Waler\n\n\nMain Partner:\n\n\t●BASF\n\t●Saint Gobain - Isover\n\t●Knauf Insulation\n\t●Rockwool\n\t●Stiferite\n\t●Eni Versalis\n\n\nPartner:\n\n\t\t●EJOT\n\n\nPartner Tecnico:\n\n\t\t●AIPE\n\t\t●FIVRA";
        static string TXT_PERCHECONSORZIO = "Il Consorzio Cortexa è il soggetto referente protagonista istituzionale della cultura del Sistema a Cappotto, organizzato e composto da più aziende con comprovata esperienza pluriennale che condividono una etica professionale, competenze tecniche specialistiche ed un progetto con finalità comuni.\n\nCortexa nasce con l'obiettivo di diffondere in Italia la cultura del Sistema di Isolamento a Cappotto di qualità, sia in termini di sistema che di progettazione e applicazione.";

        bool FirstOpen = false;
        bool SecondOpen = false;
        bool ThirdOpen = false;


        UIImage plus = UIImage.FromFile("Plus.png");
        UIImage minus = UIImage.FromFile("Minus.png");

        UIScrollView scrollview;

        CGSize size1, size2, size3;

        float H = 176;

        UITableView table;

        public int yy = 0;

        float W, HH, WApp, HApp;
        public bool change = false;

        public static ConsorzioPageController Instance { private set; get; }
        public ConsorzioPageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ConsorzioPageController.Instance = this;

            int PageGiusta = 2;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            yy = 0;

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    HH = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    HH = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    HH = HApp - 64;
                }
                else
                {
                    W = HApp;
                    HH = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, HH);

            scrollview = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            size1 = UIStringDrawing.StringSize(TXT_CHISIAMO, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width -20, 2000));
            size2 = UIStringDrawing.StringSize(TXT_CONSORZIATI, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width - 20, 2000));
            size3 = UIStringDrawing.StringSize(TXT_PERCHECONSORZIO, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width - 20, 2000));

            table = new UITableView(new CGRect(10, yy + 10, ContentView.Frame.Width - 20, 176));
            table.Source = new TableSourceConsorzio((float)ContentView.Frame.Width - 20, this);
            table.SeparatorColor = UIColor.Clear;
            table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            table.BackgroundColor = UIColor.White;
            table.ScrollEnabled = false;

            yy += 187;

            scrollview.Add(table);
            ContentView.Add(scrollview);

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy);

            /*
            Label1.Font = UIFont.FromName("OpenSans", 17);
            Label2.Font = UIFont.FromName("OpenSans", 17);
            Label3.Font = UIFont.FromName("OpenSans", 17);

            var app1 = Exp1.Frame;
            app1.Size = new CGSize(ContentView.Frame.Width,0);
            Exp1.Frame = app1;
            Exp1.Text = "";
            Exp1.Font = UIFont.FromName("OpenSans", 14);

            var app2 = Exp2.Frame;
            app2.Size = new CGSize(ContentView.Frame.Width, 0);
            Exp2.Frame = app2;
            Exp2.Text = "";
            Exp2.Font = UIFont.FromName("OpenSans", 14);

            var app3 = Exp3.Frame;
            app3.Size = new CGSize(ContentView.Frame.Width, 0);
            Exp3.Frame = app2;
            Exp3.Text = "";
            Exp3.Font = UIFont.FromName("OpenSans", 14);


            UITapGestureRecognizer Click1Tap = new UITapGestureRecognizer(() =>
            {
                ExpandView(0);
            });
            Click1.UserInteractionEnabled = true;
            Click1.AddGestureRecognizer(Click1Tap);

            UITapGestureRecognizer Click2Tap = new UITapGestureRecognizer(() =>
            {

            });
            Click2.UserInteractionEnabled = true;
            Click2.AddGestureRecognizer(Click2Tap);

            UITapGestureRecognizer Click3Tap = new UITapGestureRecognizer(() =>
            {

            });
            Click3.UserInteractionEnabled = true;
            Click3.AddGestureRecognizer(Click3Tap);


            var app4 = ExampleView.Frame;
            app4.Size = new CGSize(ContentView.Frame.Width, 176);
            app4.X = 0;
            app4.Y = 0;
            ExampleView.Frame = new CGRect(0,0,ContentView.Frame.Width, 176);

            scrollview.Add(ExampleView);

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width,H);

            //ExampleView.RemoveFromSuperview();
            ContentView.Add(scrollview);

            Console.WriteLine(ExampleView.Frame.ToString());

            ExampleView.SetNeedsDisplay();
            scrollview.SetNeedsDisplay();
            ContentView.SetNeedsDisplay();
            View.SetNeedsDisplay();
            */

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;

        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Home");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public void OpenChange(float h)
        {

            CGRect frame = table.Frame;
            frame.Height += h;
            table.Frame = frame;

            //CGRect frame2 = BottoneMostreTemp.Frame;
            //frame2.Y += h;
            //BottoneMostreTemp.Frame = frame2;

            yy += (int)h;

            scrollview.ContentSize = new CGSize(View.Frame.Width, yy + 10);

        }
        public void CloseChange(float h)
        {

            CGRect frame = table.Frame;
            frame.Height -= h;
            table.Frame = frame;

            //CGRect frame2 = BottoneMostreTemp.Frame;
            //frame2.Y -= h;
            //BottoneMostreTemp.Frame = frame2;

            yy -= (int)h;

            scrollview.ContentSize = new CGSize(View.Frame.Width, yy + 10);

        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }
    }
}