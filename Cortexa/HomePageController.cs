using Foundation;
using Xamarin.SWRevealViewController;
using System;
using UIKit;
using CoreGraphics;
using System.Threading.Tasks;
using RestSharp;

namespace Cortexa
{
    public partial class HomePageController : UIViewController
    {
        static string CONSORZIO = "CONSORZIO";
        static string NEWS = "NEWS";
        static string VIDEO = "VIDEO";
        static string WEBINAR = "WEBINAR";
        static string PARTNERS = "PARTNERS";
        static string MANUALE = "MANUALE";
        
        double Widht, Height, PaddingX, PaddingY;

        public float RevealWidht;

        public static HomePageController Instance { private set; get; }

        public HomePageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            HomePageController.Instance = this;

            int PageGiusta = 0;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController CP = storyboard.InstantiateViewController("Consorzio");
            UIViewController NP = storyboard.InstantiateViewController("News");
            UIViewController VP = storyboard.InstantiateViewController("Video");
            UIViewController WP = storyboard.InstantiateViewController("Webinar");
            UIViewController PP = storyboard.InstantiateViewController("Partner");
            UIViewController MP = storyboard.InstantiateViewController("Manuale");

            Widht = (View.Frame.Width - 75) / 2;
            Height = Widht;

            var size = UIStringDrawing.StringSize(CONSORZIO, UIFont.FromName("OpenSans-Bold", 15), new CGSize(ContentView.Frame.Width/2, 2000));

            PaddingX = 25;
            PaddingY = (ContentView.Frame.Height - ((Height + size.Height)*3)) / 4;

            Console.WriteLine(View.Frame.Width + "|" + ContentView.Frame.Height + "|" + Widht + "|" + Height + "|" + PaddingX + "|" + PaddingY + "|");

            UIButton ConsorzioButton = new UIButton(new CGRect(PaddingX, PaddingY, Widht, Height));
            UIImageView ConsorzioImage = new UIImageView(ConsorzioButton.Bounds);
            ConsorzioImage.Image = UIImage.FromFile("Consorzio.png");
            ConsorzioButton.AddSubview(ConsorzioImage);
            ConsorzioButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(CP, true);
            };

            UILabel ConsorzioLabel = new UILabel(new CGRect(25, (Height) + (PaddingY) , Widht, size.Height));
            ConsorzioLabel.Text = CONSORZIO;
            ConsorzioLabel.TextColor = UIColor.FromRGB(79,79,79);
            ConsorzioLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            ConsorzioLabel.TextAlignment = UITextAlignment.Center;


            UIButton NewsButton = new UIButton(new CGRect((PaddingX*2) + Widht, PaddingY, Widht, Height));
            UIImageView NewsImage = new UIImageView(NewsButton.Bounds);
            NewsImage.Image = UIImage.FromFile("News.png");
            NewsButton.AddSubview(NewsImage);
            NewsButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(NP, true);
            };

            UILabel NewsLabel = new UILabel(new CGRect(ContentView.Frame.Width / 2 +12, (Height) + (PaddingY), Widht, size.Height));
            NewsLabel.Text = NEWS;
            NewsLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            NewsLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            NewsLabel.TextAlignment = UITextAlignment.Center;


            UIButton VideoButton = new UIButton(new CGRect(PaddingX, (PaddingY*2) + (Height) + (size.Height) , Widht, Height));
            UIImageView VideoImage = new UIImageView(VideoButton.Bounds);
            VideoImage.Image = UIImage.FromFile("Video.png");
            VideoButton.AddSubview(VideoImage);
            VideoButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(VP, true);
            };

            UILabel VideoLabel = new UILabel(new CGRect(25, (Height * 2) + (PaddingY * 2)  + (size.Height), Widht, size.Height));
            VideoLabel.Text = VIDEO;
            VideoLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            VideoLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            VideoLabel.TextAlignment = UITextAlignment.Center;


            UIButton WebinarButton = new UIButton(new CGRect((PaddingX * 2) + Widht, (PaddingY * 2) + (Height) + (size.Height) , Widht, Height));
            UIImageView WebinarImage = new UIImageView(WebinarButton.Bounds);
            WebinarImage.Image = UIImage.FromFile("Webinar.png");
            WebinarButton.AddSubview(WebinarImage);
            WebinarButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(WP, true);
            };

            UILabel WebinarLabel = new UILabel(new CGRect(ContentView.Frame.Width / 2 +12, (Height * 2) + (PaddingY * 2)  + (size.Height), Widht, size.Height));
            WebinarLabel.Text = WEBINAR;
            WebinarLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            WebinarLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            WebinarLabel.TextAlignment = UITextAlignment.Center;


            UIButton PartnerButton = new UIButton(new CGRect(PaddingX, (PaddingY * 3) + (Height*2) + (size.Height*2) , Widht, Height));
            UIImageView PartnerImage = new UIImageView(PartnerButton.Bounds);
            PartnerImage.Image = UIImage.FromFile("Partner.png");
            PartnerButton.AddSubview(PartnerImage);
            PartnerButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(PP, true);
            };

            UILabel PartnerLabel = new UILabel(new CGRect(25, (Height * 3) + (PaddingY * 3) + (size.Height*2), Widht, size.Height));
            PartnerLabel.Text = PARTNERS;
            PartnerLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            PartnerLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            PartnerLabel.TextAlignment = UITextAlignment.Center;


            UIButton ManualeButton = new UIButton(new CGRect((PaddingX * 2) + Widht, (PaddingY * 3) + (Height * 2) + (size.Height * 2) , Widht, Height));
            UIImageView ManualeImage = new UIImageView(ManualeButton.Bounds);
            ManualeImage.Image = UIImage.FromFile("Documenti.png");
            ManualeButton.AddSubview(ManualeImage);
            ManualeButton.TouchUpInside += (object sender, EventArgs e) => {
                this.RevealViewController().PushFrontViewController(MP, true);
            };

            UILabel ManualeLabel = new UILabel(new CGRect(ContentView.Frame.Width / 2+12, (Height * 3) + (PaddingY * 3) + (size.Height * 2), Widht, size.Height));
            ManualeLabel.Text = MANUALE;
            ManualeLabel.TextColor = UIColor.FromRGB(79, 79, 79);
            ManualeLabel.Font = UIFont.FromName("OpenSans-Bold", 15);
            ManualeLabel.TextAlignment = UITextAlignment.Center;


            ContentView.Add(ConsorzioButton);
            ContentView.Add(ConsorzioLabel);
            ContentView.Add(NewsButton);
            ContentView.Add(NewsLabel);
            ContentView.Add(VideoButton);
            ContentView.Add(VideoLabel);
            ContentView.Add(WebinarButton);
            ContentView.Add(WebinarLabel);
            ContentView.Add(PartnerButton);
            ContentView.Add(PartnerLabel);
            ContentView.Add(ManualeButton);
            ContentView.Add(ManualeLabel);


            bool push = NSUserDefaults.StandardUserDefaults.BoolForKey("isCortexaNotification");
            Console.WriteLine("PUSH:" + push);
            if (push)
            {
                NSUserDefaults.StandardUserDefaults.SetBool(false,"isCortexaNotification");

                string type = NSUserDefaults.StandardUserDefaults.StringForKey("TypeCortexaNotification");
                UIViewController SHP = storyboard.InstantiateViewController("SondaggioHome");

                if (type == "sondaggio")
                    this.RevealViewController().PushFrontViewController(SHP, true);
                if (type == "news")
                    this.RevealViewController().PushFrontViewController(NP, true);
                if (type == "manuale")
                    this.RevealViewController().PushFrontViewController(MP, true);
                if (type == "corso")
                    this.RevealViewController().PushFrontViewController(WP, true);
                if (type == "video")
                    this.RevealViewController().PushFrontViewController(VP, true);


            }

            if (!AppDelegate.Instance.PushSend)
            {
                if (!string.IsNullOrWhiteSpace(AppDelegate.Instance.APNToken))
                {

                    string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa");

                    var client = new RestClient("http://www.cortexa.it/");

                    var request = new RestRequest("cortexa-app/tokenpush.php?token=" + token + "&device=ios&push=" + AppDelegate.Instance.APNToken, Method.GET);


                    client.ExecuteAsync(request, (s, e) =>
                    {
                    });
                }

                AppDelegate.Instance.PushSend = true;
            }



            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            AppDelegate.Instance.RevealWidht = (float)this.RevealViewController().RightViewRevealWidth;
            MenuItem.Clicked += (sender, e) => this.RevealViewController().RevealToggleAnimated(true);
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }
    }
}