using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class NewsPageController : UIViewController
    {

        List<NewsObject> ListNews;
        List<UIView> listView = new List<UIView>();

        public NewsObject NewsSelected;

        float W, H, WApp, HApp;
        public bool change = false;

        public static NewsPageController Instance { private set; get; }

        public NewsPageController (IntPtr handle) : base (handle)
        {
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            int PageGiusta = 3;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            NewsPageController.Instance = this;

            ListNews = AppDelegate.Instance.ListNews;
            listView.Clear();

            UIScrollView scrollview = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            int yy = 0;

            var size = UIStringDrawing.StringSize("\na\na\na", UIFont.FromName("OpenSans", 15), new CGSize(ContentView.Frame.Width - 40, 2000));

            foreach (NewsObject no in ListNews)
            {
                int yyy = 0;

                UIView container = new UIView(new CGRect(20, yy + 10, ContentView.Frame.Width - 40, 150));
                container.BackgroundColor = UIColor.FromRGB(235, 235, 235);

                UIImageView image = new UIImageView(new CGRect(container.Frame.Width / 2 - 50, yyy + 5, 100, 100));
                image.Image = UIImage.FromFile("placeholder.png");

                yyy += 105;

                var size1 = UIStringDrawing.StringSize(no.titolo, UIFont.FromName("OpenSans", 17), new CGSize(container.Frame.Width-10, 2000));

                UILabel tit = new UILabel(new CGRect(5, yyy + 5, container.Frame.Width-10, size1.Height));
                tit.Text = no.titolo;
                tit.TextColor = UIColor.FromRGB(134, 58, 128);
                tit.Font = UIFont.FromName("OpenSans", 17);
                tit.Lines = 0;

                yyy += (int)size1.Height + 5;

                UILabel data = new UILabel(new CGRect(5, yyy+3, container.Frame.Width-10, 17));
                data.Text = no.data;
                data.TextColor = UIColor.Black;
                data.Font = UIFont.FromName("OpenSans-Bold", 13);

                yyy += 20;

                size1 = UIStringDrawing.StringSize(no.testo, UIFont.FromName("OpenSans", 14), new CGSize(container.Frame.Width-10, 2000));

                UILabel test;

                if (size1.Height > size.Height)
                {

                    test = new UILabel(new CGRect(5, yyy + 5, container.Frame.Width-10, size.Height));
                    test.Lines = 3;
                    yyy += (int)size.Height + 5;
                }
                else
                {
                    test = new UILabel(new CGRect(5, yyy + 5, container.Frame.Width-10, size1.Height));
                    yyy += (int)size1.Height + 5;
                }
                test.Text = no.testo;
                test.TextColor = UIColor.Black;
                test.Font = UIFont.FromName("OpenSans", 14);

                container.Frame = new CGRect(container.Frame.X, container.Frame.Y, container.Frame.Width, yyy + 5);

                if (no.Image == null)
                    SetImageAsync(image, no, no.immagine,(float)container.Frame.Width);
                else
                {

                    nfloat w = no.Image.Size.Width;
                    nfloat h = no.Image.Size.Height;

                    float newWidht, newHeight;

                    newWidht = (float)container.Frame.Width - 40;
                    newHeight = (float)((newWidht * h) / w);

                    if (newHeight > 100)
                    {

                        newHeight = (float)100;
                        newWidht = (float)((100 * w) / h);

                    }

                    Console.WriteLine(w + "  " + h);

                    image.Frame = new CGRect(container.Frame.Width/2 - (newWidht / 2), 50 - (newHeight / 2), newWidht, newHeight);


                    image.Image = no.Image;
                }


                UITapGestureRecognizer viewTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < listView.Count; ii++)
                    {
                        Console.WriteLine((listView[ii] == s.View) + "|" + ii);
                        if (listView[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    NewsSelected = ListNews[giusto];

                    UIStoryboard storyboard = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard = UIStoryboard.FromName("Main_iPad", null);
                    }
                    UIViewController NDP = storyboard.InstantiateViewController("NewsDettaglio");
                    this.RevealViewController().PushFrontViewController(NDP, true);

                });
                container.UserInteractionEnabled = true;
                container.AddGestureRecognizer(viewTap);

                listView.Add(container);

                container.Add(image);
                container.Add(tit);
                container.Add(data);
                container.Add(test);

                scrollview.Add(container);
                yy += yyy + 15;


            }

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            ContentView.Add(scrollview);


            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;

            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
                
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Home");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }


        public void SetImageAsync(UIImageView image, NewsObject obj, string url , float widht)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        nfloat w = cachedImage.Size.Width;
                        nfloat h = cachedImage.Size.Height;

                        float newWidht, newHeight;

                        newWidht = (float)widht-40;
                        newHeight = (float)((newWidht * h) / w);

                        if (newHeight > 100)
                        {

                            newHeight = (float)100;
                            newWidht = (float)((100 * w) / h);

                        }

                        Console.WriteLine(w + "  " + h);

                        image.Frame = new CGRect(widht/2 - (newWidht / 2), 55 - (newHeight / 2), newWidht, newHeight);

                        image.Image = cachedImage;
                        obj.Image = cachedImage;

                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {

            this.RevealViewController().RightViewRevealOverdraw = 0.0f;

            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked; 

            change = true;

            this.RevealViewController().ShouldAutorotateToInterfaceOrientation(toInterfaceOrientation);
            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }

    }
}