﻿using Foundation;
using RestSharp;
using System;
using System.Collections.Generic;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        // class-level declarations

        public bool FirstDownload = false;
        public List<VideoObject> ListVideo = new List<VideoObject>();
        public List<CorsiObject> ListCorsi = new List<CorsiObject>();
        public List<NewsObject> ListNews = new List<NewsObject>();
        public List<ManualeObject> ListManuale = new List<ManualeObject>();
        public List<PartnersObject> ListPartners = new List<PartnersObject>();
        public List<BannerObject> ListBanner = new List<BannerObject>();
        public List<SondaggioObject> ListSondaggio = new List<SondaggioObject>();

        public float RevealWidht;

        public string APNToken = "";
        public bool Login = false;
        public bool PushSend = false;

        public List<bool> FlagPage = new List<bool>();

        // HP:0 HPT:1 C:2 N:3 ND:4 V:5 W:6 WD:7 P:8 PD:9 M:10 S:11

        public static AppDelegate Instance { private set; get; }
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method
            AppDelegate.Instance = this;

            for (int i = 0; i < 12; i++)
                FlagPage.Add(false);

            //NOTIFICATION
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }

            if (launchOptions != null && launchOptions.Keys != null && launchOptions.Keys.Length != 0 && launchOptions.ContainsKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")))
            {
                Console.WriteLine("entro");

                NSDictionary userInfo = launchOptions.ObjectForKey(new NSString("UIApplicationLaunchOptionsRemoteNotificationKey")) as NSDictionary;
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                string type = string.Empty;


                foreach (var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("type")))
                    type = (userInfo[new NSString("type")] as NSString).ToString();

                Console.WriteLine("object:" + type);

                NSUserDefaults.StandardUserDefaults.SetString(type, "TypeCortexaNotification");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isCortexaNotification");
                
            }

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {

            //Get current device token
            var DeviceToken = deviceToken.Description;
            Console.WriteLine("PUSH TOKEN1:" + DeviceToken);
            if (!string.IsNullOrWhiteSpace(DeviceToken))
            {
                DeviceToken = DeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");
            }

            APNToken = DeviceToken;

            if (Login)
            {

                string token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa");

                var client = new RestClient("http://www.cortexa.it/");

                var request = new RestRequest("cortexa-app/tokenpush.php?token=" + token + "&device=ios&push=" + APNToken, Method.GET);


                client.ExecuteAsync(request, (s, e) =>
                {
                });

                PushSend = true;
            }

            Console.WriteLine("PUSH TOKEN2:" + APNToken);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            //This method gets called whenever the app is already running and receives a push notification
            // YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
            // this includes setting the badge, playing a sound, etc.

            Console.WriteLine("remote" + application.ApplicationState.ToString());


            if (application.ApplicationState == UIApplicationState.Inactive || application.ApplicationState == UIApplicationState.Background)
            {


                Console.WriteLine("back");

                UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

                string type = string.Empty;


                foreach( var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("type")))
                    type = (userInfo[new NSString("type")] as NSString).ToString();

                Console.WriteLine("object:" + type );

                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("Main_iPad", null);
                }

                UIViewController lp = storyboard.InstantiateViewController("SWController");

                //UIViewController NP = storyboard.InstantiateViewController("News");
                //UIViewController VP = storyboard.InstantiateViewController("Video");
                //UIViewController WP = storyboard.InstantiateViewController("Webinar");
                //UIViewController MP = storyboard.InstantiateViewController("Manuale");
                //UIViewController SHP = storyboard.InstantiateViewController("SondaggioHome");

                NSUserDefaults.StandardUserDefaults.SetString(type, "TypeCortexaNotification");
                NSUserDefaults.StandardUserDefaults.SetBool(true, "isCortexaNotification");

                TopController().PresentModalViewController(lp, true);

                /*if (type == "sondaggio")
                    TopController().RevealViewController().PushFrontViewController(SHP, true);
                if (type == "news")
                    TopController().RevealViewController().PushFrontViewController(NP, true);
                if (type == "manuale")
                    TopController().RevealViewController().PushFrontViewController(MP, true);
                if (type == "corso")
                    TopController().RevealViewController().PushFrontViewController(WP, true);
                if (type == "video")
                    TopController().RevealViewController().PushFrontViewController(VP, true);

                TopController().PresentModalViewController(lp, true);*/


            }
            if (application.ApplicationState == UIApplicationState.Active)
            {
                string type = string.Empty;


                foreach (var obj in userInfo)
                {
                    Console.WriteLine(obj.Key + ":" + obj.Value);
                }

                if (userInfo.ContainsKey(new NSString("type")))
                    type = (userInfo[new NSString("type")] as NSString).ToString();

                Console.WriteLine("object:" + type);

                UIStoryboard storyboard = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard = UIStoryboard.FromName("Main_iPad", null);
                }

                UIViewController lp = storyboard.InstantiateViewController("SWController");

                //UIViewController NP = storyboard.InstantiateViewController("News");
                //UIViewController VP = storyboard.InstantiateViewController("Video");
                //UIViewController WP = storyboard.InstantiateViewController("Webinar");
                //UIViewController MP = storyboard.InstantiateViewController("Manuale");
                //UIViewController SHP = storyboard.InstantiateViewController("SondaggioHome");

                //NSUserDefaults.StandardUserDefaults.SetString(type, "TypeCortexaNotification");
                //NSUserDefaults.StandardUserDefaults.SetBool(true, "isCortexaNotification");

                int giusto=-1;
                for (int i = 0; i < 12; i++)
                {
                    if (FlagPage[i])
                        giusto = i;
                        
                }

                
                if (giusto == 0) {
                    HomePageController.Instance.Notification(type);
                }

                if (giusto == 1)
                {
                    HomePageTabletController.Instance.Notification(type);
                }

                if (giusto == 2)
                {
                    ConsorzioPageController.Instance.Notification(type);
                }

                if (giusto == 3)
                {
                    NewsPageController.Instance.Notification(type);
                }

                if (giusto == 4)
                {
                    NewsDettaglioPageController.Instance.Notification(type);
                }

                if (giusto == 5)
                {
                    VideoPageController.Instance.Notification(type);
                }

                if (giusto == 6)
                {
                    WebinarPageController.Instance.Notification(type);
                }

                if (giusto == 7)
                {
                    WebinarDettaglioPageController.Instance.Notification(type);
                }

                if (giusto == 8)
                {
                    PartnersPageController.Instance.Notification(type);
                }

                if (giusto == 9)
                {
                    PartnersDettalioPageController.Instance.Notification(type);
                }

                if (giusto == 10)
                {
                    ManualePageController.Instance.Notification(type);
                }

                if (giusto == 11)
                {
                    SondaggioHomePageController.Instance.Notification(type);
                }

                //TopController().PresentModalViewController(lp, true);

                /*if (type == "sondaggio")
                    TopController().RevealViewController().PushFrontViewController(SHP, true);
                if (type == "news")
                    TopController().RevealViewController().PushFrontViewController(NP, true);
                if (type == "manuale")
                    TopController().RevealViewController().PushFrontViewController(MP, true);
                if (type == "corso")
                    TopController().RevealViewController().PushFrontViewController(WP, true);
                if (type == "video")
                    TopController().RevealViewController().PushFrontViewController(VP, true);

                TopController().PresentModalViewController(lp, true);*/

            }
        }


        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public UIViewController TopController()
        {
            UIViewController topController = (UIApplication.SharedApplication).KeyWindow.RootViewController;
            while (topController.PresentedViewController != null)
            {
                topController = topController.PresentedViewController;
            }

            return topController;
        }
    }
}