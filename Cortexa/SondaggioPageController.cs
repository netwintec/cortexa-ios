using Foundation;
using System;
using UIKit;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using Xamarin.SWRevealViewController;
using CoreGraphics;
using System.Threading.Tasks;

namespace Cortexa
{
    public partial class SondaggioPageController : UIViewController
    {
        float W, H, WApp, HApp;
        public bool change = false;

        int indiceDomanda = 0;

        List<SondaggioObject> ListSondaggio;
        List<UIImageView> ListImage = new List<UIImageView>();

        List<AssociazioneObject> ListResponse = new List<AssociazioneObject>();

        UILabel tit;

        UIScrollView scrollView;
        UIView MainView;

        CGSize size;

        UIImage Acceso = UIImage.FromFile("Acceso.png");
        UIImage Spento = UIImage.FromFile("Spento.png");

        bool Clicked = false;
        int Selected = 0;
        public SondaggioPageController (IntPtr handle) : base (handle)
        {
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            Load.StartAnimating();

            ListSondaggio = AppDelegate.Instance.ListSondaggio;

            scrollView = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            string titolo = "TITOLO";
            size = UIStringDrawing.StringSize(titolo, UIFont.FromName("OpenSans", 17), new CGSize(ContentView.Frame.Width - 20, 2000));

            tit = new UILabel(new CGRect(ContentView.Frame.Width / 2 - size.Width / 2, 10, size.Width, size.Height));
            tit.Text = titolo;
            tit.Font = UIFont.FromName("OpenSans", 17);
            tit.TextColor = UIColor.Black;
            tit.Lines = 0;
            tit.TextAlignment = UITextAlignment.Left;


            CreateView();

            ContentView.Add(tit);

            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("SondaggioHome");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }


        public void SendResponse()
        {
            int finiti = 0;
            int ErrFiniti = 0;

            foreach (var a in ListResponse)
            {
                //***** DOWNLOAD SONDAGGIO ****
                var clientSondaggio = new RestClient("http://www.cortexa.it/");

                var requestSondaggio = new RestRequest("cortexa-app/sondaggi_set.php?token=" + NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa")+ "&id="+a.sppolls_poll_id+"&risposta="+a.poll, Method.GET);

                //sondaggi_set.php?token=c839740856c9dd291bc01058facb1b4a&id=1&risposta=Risposta 2

                clientSondaggio.ExecuteAsync(requestSondaggio, (s, e) =>
                {

                    Console.WriteLine("Result Sondaggio:" + s.StatusCode + "|" + s.Content);

                    if (s.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        finiti++;

                    }

                    else
                    {
                        finiti++;
                        ErrFiniti++;
                    }

                    if ((finiti + 1) == ListResponse.Count)
                    {
                        InvokeOnMainThread(() => { 
                            if ((ErrFiniti + 1) == ListResponse.Count)
                            {
                                FinishError();
                            }
                            else
                            {
                                Finish();
                            }
                        });
                    }
                });
            }

        }


        public void CreateView() {

            int yy = 0;

            MainView = new UIView();

            Clicked = false;
            ListImage.Clear();
            SondaggioObject so = ListSondaggio[indiceDomanda];

            var size1 = UIStringDrawing.StringSize((indiceDomanda+1)+") "+so.title, UIFont.FromName("OpenSans", 16), new CGSize(ContentView.Frame.Width - 20, 2000));

            UILabel dom = new UILabel(new CGRect(10, yy+10, size1.Width, size1.Height));
            dom.Text = (indiceDomanda + 1) + ") " + so.title;
            dom.Font = UIFont.FromName("OpenSans", 16);
            dom.TextColor = UIColor.Black;
            dom.Lines = 0;
            dom.TextAlignment = UITextAlignment.Left;

            MainView.Add(dom);

            yy += (int)size1.Height + 10;


            foreach(var a in so.polls)
            {

                UIImageView image = new UIImageView(new CGRect(30, yy +10, 20, 20));
                image.Image = Spento;

                size1 = UIStringDrawing.StringSize(a.poll, UIFont.FromName("OpenSans", 14), new CGSize(ContentView.Frame.Width - 90, 2000));

                UILabel risp = new UILabel(new CGRect(55, yy + 10, size1.Width, size1.Height));
                risp.Text = a.poll;
                risp.Font = UIFont.FromName("OpenSans", 14);
                risp.TextColor = UIColor.Black;
                risp.Lines = 0;
                risp.TextAlignment = UITextAlignment.Left;


                UITapGestureRecognizer imgTap = new UITapGestureRecognizer((s) =>
                {
                    int giusto = -1;
                    for (int ii = 0; ii < ListImage.Count; ii++)
                    {
                        Console.WriteLine((ListImage[ii] == s.View) + "|" + ii);
                        if (ListImage[ii] == s.View)
                        {
                            giusto = ii;
                        }
                    }

                    if (ListImage[giusto].Image == Spento)
                    {
                        foreach (var i in ListImage)
                        {
                            if (i.Image == Acceso)
                            {
                                i.Image = Spento;
                            }
                        }

                        ListImage[giusto].Image = Acceso;
                        Clicked = true;

                        Selected = giusto;
                    }

                });
                image.UserInteractionEnabled = true;
                image.AddGestureRecognizer(imgTap);

                ListImage.Add(image);

                MainView.Add(image);
                MainView.Add(risp);


                if (size1.Height > 20)
                    yy += (int)size1.Height + 10;
                else
                    yy += 30;
            }


            UILabel Tot = new UILabel(new CGRect(10, yy + 13, ContentView.Frame.Width -20, 17));
            Tot.Text = (indiceDomanda+1)+"/"+ListSondaggio.Count;
            Tot.Font = UIFont.FromName("OpenSans", 14);
            Tot.TextColor = UIColor.Black;
            Tot.TextAlignment = UITextAlignment.Center;

            MainView.Add(Tot);

            yy += 30;


            UIView Contorno = new UIView(new CGRect(ContentView.Frame.Width / 2 - 75, yy + 15, 150, 50));
            Contorno.BackgroundColor = UIColor.FromRGB(138, 138, 138);
            Contorno.Layer.CornerRadius = 5;

            UIView Interno = new UIView(new CGRect(2, 2, 146, 46));
            Interno.BackgroundColor = UIColor.White;
            Interno.Layer.CornerRadius = 4;

            UIButton btn = new UIButton(new CGRect(2, 2, 142, 42));
            if((indiceDomanda+1) == ListSondaggio.Count)
                btn.SetTitle("Invia Dati", UIControlState.Normal);
            else
                btn.SetTitle("Avanti", UIControlState.Normal);
            btn.SetTitleColor(UIColor.FromRGB(138, 138, 138), UIControlState.Normal);
            btn.Font = UIFont.FromName("OpenSans", 16);
            btn.TouchUpInside += delegate {
                if (Clicked)
                {
                    if ((indiceDomanda + 1) < ListSondaggio.Count)
                    {

                        AssociazioneObject ao = new AssociazioneObject();
                        ao.sppolls_poll_id = ListSondaggio[indiceDomanda].sppolls_poll_id;
                        ao.poll = ListSondaggio[indiceDomanda].polls[Selected].poll;

                        ListResponse.Add(ao);


                        MainView.RemoveFromSuperview();
                        indiceDomanda++;
                        CreateView();

                    }
                    else
                    {
                        if ((indiceDomanda + 1) == ListSondaggio.Count)
                        {
                            AssociazioneObject ao = new AssociazioneObject();
                            ao.sppolls_poll_id = ListSondaggio[indiceDomanda].sppolls_poll_id;
                            ao.poll = ListSondaggio[indiceDomanda].polls[Selected].poll;

                            ListResponse.Add(ao);

                            indiceDomanda++;
                        }

                        if ((indiceDomanda) == ListSondaggio.Count)
                        {

                            LoadView.Alpha = 1;
                            SendResponse();
                        }
                    }
                }

            };

            Interno.Add(btn);
            Contorno.Add(Interno);

            MainView.Add(Contorno);

            yy += 65;

            MainView.Frame = new CGRect(0, size.Height + 10, ContentView.Frame.Width, yy);

            ContentView.Add(MainView);

            scrollView.ContentSize = new CGSize(ContentView.Frame.Width, yy + size.Height + 20);

        }

        public async void FinishError()
        {
            int button = await ShowAlert("Errore", "Invio dei risultati non riuscito riprovare.", "Ok");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                LoadView.Alpha = 0;

            }
        }

        public async void Finish()
        {
            int button = await ShowAlert("Sondaggio", "Sondaggio completato con successo", "Ok");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                UIStoryboard storyboard2 = new UIStoryboard();
                if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                {
                    storyboard2 = UIStoryboard.FromName("Main", null);
                }
                else
                {
                    storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                }

                UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");
                this.RevealViewController().PushFrontViewController(SHP, true);

            }
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

    }
}