using CoreGraphics;
using Foundation;
using System;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class WebinarDettaglioPageController : UIViewController
    {

        CorsiObject co;

        float W, H, WApp, HApp;
        public bool change = false;

        public static WebinarDettaglioPageController Instance { private set; get; }

        public WebinarDettaglioPageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            WebinarDettaglioPageController.Instance = this;

            int PageGiusta = 7;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);
            RequisitiView.Frame = new CGRect(0, 64, W, H);

            co = WebinarPageController.Instance.CorsoSelected;

            //***** REQUISITI VIEW ****//

            UIScrollView sv = new UIScrollView(new CGRect(0, 0, TextView.Frame.Width, TextView.Frame.Height));

            var size = UIStringDrawing.StringSize(co.Dettagli.requisiti, UIFont.FromName("OpenSans", 16), new CGSize(TextView.Frame.Width - 10, 2000));

            UILabel txt = new UILabel(new CGRect(5,5,TextView.Frame.Width - 10 , size.Height));
            txt.TextColor = UIColor.Black;
            txt.Lines = 0;
            txt.Text = co.Dettagli.requisiti;
            txt.Font = UIFont.FromName("OpenSans", 16);

            sv.Add(txt);

            sv.ContentSize = new CGSize(TextView.Frame.Width, size.Height + 10);
            TextView.Add(sv);

            TextView.Layer.CornerRadius = 4;

            ChiudiView.Layer.CornerRadius = 20;
            ChiudiButton.TouchUpInside += delegate {
                RequisitiView.Alpha = 0;
            };

            //****** NORMAL VIEW *****//
            UIScrollView scrollview = new UIScrollView(new CGRect(0, 0, ContentView.Frame.Width, ContentView.Frame.Height));

            int yy = 0;

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            UILabel Corso = new UILabel(new CGRect(10, yy + 10, ContentView.Frame.Width - 20, 21));
            Corso.Text = co.Dettagli.numero;
            Corso.TextColor = UIColor.FromRGB(35, 78, 139);
            Corso.Font = UIFont.FromName("OpenSans-Bold", 18);

            yy += 31;

            size = UIStringDrawing.StringSize(co.Dettagli.titolo, UIFont.FromName("OpenSans", 17), new CGSize(ContentView.Frame.Width - 20, 2000));

            UILabel tit = new UILabel(new CGRect(10, yy + 5, ContentView.Frame.Width - 20, size.Height));
            tit.Text = co.Dettagli.titolo;
            tit.TextColor = UIColor.FromRGB(35, 78, 139);
            tit.Font = UIFont.FromName("OpenSans", 17);
            tit.Lines = 0;

            yy += (int)size.Height +5;

            UIView separator = new UIView(new CGRect(10, yy + 5, ContentView.Frame.Width - 20, 1));
            separator.BackgroundColor = UIColor.Black;

            yy += 6;

            size = UIStringDrawing.StringSize(co.Dettagli.descrizione, UIFont.FromName("OpenSans", 15), new CGSize(ContentView.Frame.Width - 20, 2000));

            UILabel desc = new UILabel(new CGRect(10, yy + 10, ContentView.Frame.Width - 20, size.Height));
            desc.Text = co.Dettagli.descrizione;
            desc.TextColor = UIColor.Black;
            desc.Font = UIFont.FromName("OpenSans", 15);
            desc.Lines = 0;

            yy += (int)(size.Height + 10);


            UILabel req = new UILabel(new CGRect(10, yy + 15, ContentView.Frame.Width - 20, 20));
            req.Text = "Requisiti minimi accessibilità";
            req.TextColor = UIColor.FromRGB(35, 78, 139);
            req.Font = UIFont.FromName("OpenSans", 17);
            req.TextAlignment = UITextAlignment.Center;

            yy += (int)(36);

            UIView Contorno = new UIView(new CGRect(ContentView.Frame.Width / 2 - 50, yy + 10, 100, 40));
            Contorno.BackgroundColor = UIColor.FromRGB(35, 78, 139);
            Contorno.Layer.CornerRadius = 5;

            UIView Dentro = new UIView(new CGRect(2, 2, 96, 36));
            Dentro.BackgroundColor = UIColor.White;
            Dentro.Layer.CornerRadius = 4;

            UIButton btn = new UIButton(new CGRect(2, 2, 92, 32));
            btn.SetTitle("Leggi", UIControlState.Normal);
            btn.SetTitleColor(UIColor.FromRGB(35, 78, 139), UIControlState.Normal);
            btn.Font = UIFont.FromName("OpenSans", 16);
            btn.TouchUpInside += delegate {
                RequisitiView.Alpha = 1;
                sv.ScrollRectToVisible(new CGRect(0, 0, 1, 1), false);
            };

            Dentro.Add(btn);
            Contorno.Add(Dentro);

            yy += 50;

            scrollview.Add(Corso);
            scrollview.Add(tit);
            scrollview.Add(separator);
            scrollview.Add(desc);
            scrollview.Add(req);
            scrollview.Add(Contorno);

            if (co.abilitato == "1")
            {

                UIView Container = new UIView(new CGRect(ContentView.Frame.Width / 2 - 75, yy + 20, 150, 40));
                Container.BackgroundColor = UIColor.FromRGB(35, 78, 139);
                Container.Layer.CornerRadius = 5;

                UIImageView image = new UIImageView(new CGRect(5, 8, 30, 24));
                image.Image = UIImage.FromFile("Iscriviti.png");

                UILabel isc = new UILabel(new CGRect(45, 10, Container.Frame.Width - 50, 20));
                isc.Text = "ISCRIVITI";
                isc.TextColor = UIColor.White;
                isc.Font = UIFont.FromName("OpenSans", 16);
                isc.TextAlignment = UITextAlignment.Center;


                UITapGestureRecognizer ContainerTap = new UITapGestureRecognizer((s) =>
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(co.Dettagli.url_iscrizione));
                });
                Container.UserInteractionEnabled = true;
                Container.AddGestureRecognizer(ContainerTap);

                Container.Add(image);
                Container.Add(isc);

                yy += 60;

                scrollview.Add(Container);
            }

            scrollview.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

            ContentView.Add(scrollview);



            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Webinar");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }


        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }

    }
}