﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using System.Text.RegularExpressions;
using System.Text;

using Newtonsoft.Json.Linq;
using RestSharp;
using Newtonsoft.Json;
using System.Threading;

namespace Cortexa
{
    public partial class ViewController : UIViewController
    {

        int intDisplayWidth;
        int intDisplayHeight;

        List<UIWebView> ListWebView = new List<UIWebView>();
        List<string> ListUrl = new List<string>();


        public string token;

        int LoadFiniti = 0;
        int LoadTotali = 6;

        UIStoryboard storyboard;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            base.NavigationController.NavigationBarHidden = true;

            this.ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation.Portrait);

            storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }

            token = NSUserDefaults.StandardUserDefaults.StringForKey("TokenCortexa");
            Console.WriteLine("TOKEN"+token);

            Load.StartAnimating();

            ButtonCorner.Layer.CornerRadius = 5;
            Login.Layer.CornerRadius = 4;

            Login.Font = UIFont.FromName("OpenSans",16);
            Utente.Font = UIFont.FromName("OpenSans", 16);
            Password.Font = UIFont.FromName("OpenSans", 16);
            Registrati.Font = UIFont.FromName("OpenSans", 16);

            Password.SecureTextEntry = true;

            Utente.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                return true;

            };

            Password.ShouldReturn += (UITextField) =>
            {

                UITextField.ResignFirstResponder();
                return true;

            };

            Registrati.TouchUpInside += delegate {

                UIApplication.SharedApplication.OpenUrl(new NSUrl("http://www.cortexa.it/it/area-riservata/registrazione.html"));

            };

            Login.TouchUpInside += delegate {

                if (String.IsNullOrWhiteSpace(Utente.Text) || String.IsNullOrWhiteSpace(Password.Text))
                {

                    var adErr = new UIAlertView("Login Non Riuscito",
                                    "E-Mail o Password non inseriti",
                                    null, "OK", null);
                    adErr.Show();

                }
                else
                {
                    LoadView.Alpha = 1;

                    var clientLogin = new RestClient("http://www.cortexa.it/");

                    var requestLogin = new RestRequest("cortexa-app/login.php?user=" + Utente.Text + "&pass=" + Password.Text, Method.GET);


                    clientLogin.ExecuteAsync(requestLogin, (s, e) =>
                    {

                        Console.WriteLine("Result Login:" + s.StatusCode + "|" + s.Content);

                        if (s.StatusCode == System.Net.HttpStatusCode.OK)
                        {

                            if (!String.IsNullOrWhiteSpace(s.Content))
                            {

                                token = s.Content;// "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

                                NSUserDefaults.StandardUserDefaults.SetString(token,"TokenCortexa");


                                if (AppDelegate.Instance.FirstDownload)
                                {
                                    InvokeOnMainThread(() =>
                                    {

                                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                                        this.PresentModalViewController(lp, true);

                                    });
                                }
                                else
                                {
                                    InvokeOnMainThread(() =>
                                    {
                                        LoadData();
                                    });
                                }
                            }
                            else
                            {
                                InvokeOnMainThread(() =>
                                {
                                    LoadView.Alpha = 0;

                                    Utente.Text = "";
                                    Password.Text = "";
                                    var adErr = new UIAlertView("Login Non Riuscito",
                                        "E-Mail o Password Errata",
                                        null, "OK", null);
                                    adErr.Show();
                                });
                            }
                        }
                        else
                        {
                            InvokeOnMainThread(() =>
                            {
                                LoadView.Alpha = 0;

                                Utente.Text = "";
                                Password.Text = "";
                                var adErr = new UIAlertView("Login Non Riuscito",
                                        "E-Mail o Password Errata",
                                        null, "OK", null);
                                adErr.Show();
                            });
                        }

                    });
                }

            };


            if (!string.IsNullOrWhiteSpace(token)) {

                LoadView.Alpha = 1;

                var clientCheck = new RestClient("http://www.cortexa.it/");

                var requestCheck = new RestRequest("cortexa-app/banner.php?token=" + token, Method.GET);


                clientCheck.ExecuteAsync(requestCheck, (s, e) =>
                {

                    Console.WriteLine("Result Check:" + s.StatusCode + "|" + s.Content);

                    if (s.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        InvokeOnMainThread(() =>
                        {
                            LoadData();
                        });

                    }
                    else
                    {
                        InvokeOnMainThread(() =>
                        {
                            LoadView.Alpha = 0;
                        });
                    }

                });

            }

            /************ FONT ************
            OpenSans
            OpenSans-Bold
            ******************************/

            /*var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;

            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family; {0} \n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);

                var fontNames = UIFont.FontNamesForFamilyName(familyName);

                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                }

                // Perform any additional setup after loading the view, typically from a nib.
            }*/


            

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }


        public void LoadData()
        {

            //******* DOWNLOAD VIDEO *****
            var clientVideo = new RestClient("http://www.cortexa.it/");

            var requestVideo = new RestRequest("cortexa-app/video.php?token=" + token, Method.GET);


            clientVideo.ExecuteAsync(requestVideo, (s, e) =>
            {

                Console.WriteLine("Result Video:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListVideo = JsonConvert.DeserializeObject<List<VideoObject>>(s.Content);
                    LoadFiniti++;
                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });


            //***** DOWNLOAD CORSI ****
            var clientCorsi = new RestClient("http://www.cortexa.it/");

            var requestCorsi = new RestRequest("cortexa-app/corsi.php?token=" + token, Method.GET);


            clientCorsi.ExecuteAsync(requestCorsi, (s, e) =>
            {

                Console.WriteLine("Result Corsi:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListCorsi = JsonConvert.DeserializeObject<List<CorsiObject>>(s.Content);

                    InvokeOnMainThread(() =>
                    {
                        for (int i = 0; i < AppDelegate.Instance.ListCorsi.Count; i++)
                        {

                            var clientCorsi2 = new RestClient("http://www.cortexa.it/");

                            var requestCorsi2 = new RestRequest("cortexa-app/dettagli_corso.php?id=" + AppDelegate.Instance.ListCorsi[i].id + "&token=" + token, Method.GET);

                            IRestResponse response = clientCorsi2.Execute(requestCorsi2);

                            Console.WriteLine("Result Corso " + AppDelegate.Instance.ListCorsi[i].id + ":" + response.StatusCode + "|" + response.Content);

                            if (s.StatusCode == System.Net.HttpStatusCode.OK)
                            {

                                List<CorsiDettagliObject> cdo = new List<CorsiDettagliObject>();

                                cdo = JsonConvert.DeserializeObject<List<CorsiDettagliObject>>(response.Content);

                                AppDelegate.Instance.ListCorsi[i].Dettagli = cdo[0];

                            }

                        }

                    });

                    LoadFiniti++;

                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });


            //***** DOWNLOAD NEWS ****
            var clientNews = new RestClient("http://www.cortexa.it/");

            var requestNews = new RestRequest("cortexa-app/news.php?token=" + token, Method.GET);


            clientNews.ExecuteAsync(requestNews, (s, e) =>
            {

                Console.WriteLine("Result News:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListNews = JsonConvert.DeserializeObject<List<NewsObject>>(s.Content);

                    LoadFiniti++;

                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });


            //***** DOWNLOAD MANUALE ****
            var clientManuale = new RestClient("http://www.cortexa.it/");

            var requestManuale = new RestRequest("cortexa-app/manuale.php?token=" + token, Method.GET);


            clientManuale.ExecuteAsync(requestManuale, (s, e) =>
            {

                Console.WriteLine("Result Manuale:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListManuale = JsonConvert.DeserializeObject<List<ManualeObject>>(s.Content);

                    LoadFiniti++;

                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });

            //***** DOWNLOAD PARTNERS ****
            var clientPartners = new RestClient("http://www.cortexa.it/");

            var requestPartners = new RestRequest("cortexa-app/partner.php?token=" + token, Method.GET);


            clientPartners.ExecuteAsync(requestPartners, (s, e) =>
            {

                Console.WriteLine("Result Partners:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListPartners = JsonConvert.DeserializeObject<List<PartnersObject>>(s.Content);

                    LoadFiniti++;

                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });

            //***** DOWNLOAD BANNER ****
            var clientBanner = new RestClient("http://www.cortexa.it/");

            var requestBanner = new RestRequest("cortexa-app/banner.php?token=" + token, Method.GET);


            clientBanner.ExecuteAsync(requestBanner, (s, e) =>
            {

                Console.WriteLine("Result Banner:" + s.StatusCode + "|" + s.Content);

                if (s.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    AppDelegate.Instance.ListBanner = JsonConvert.DeserializeObject<List<BannerObject>>(s.Content);

                    foreach(var b in AppDelegate.Instance.ListBanner)
                    {
                        b.image = UIImage.FromFile("placeholder.png");
                        SetImageAsync(b);
                    }

                    LoadFiniti++;

                }

                if (LoadFiniti == LoadTotali)
                {
                    InvokeOnMainThread(() =>
                    {
                        AppDelegate.Instance.Login = true;

                        UIViewController lp = storyboard.InstantiateViewController("SWController");
                        this.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
                        this.PresentModalViewController(lp, true);
                    });
                }

            });
        }

        public void SetImageAsync(BannerObject obj)
        {
            UIImage Placeholder = UIImage.FromFile("placeholder.png");


            UIImage cachedImage;
            try
            {
                ThreadPool.QueueUserWorkItem((t) =>
                {

                    // retrive the image and create a local scaled thumbnail; return a UIImage object of the thumbnail                      
                    //cachedImage = ricetteImages.GetImage(entry.Immagine, true) ?? Placeholder;
                    cachedImage = FromUrl(obj.url) ?? Placeholder;


                    InvokeOnMainThread(() =>
                    {
                        obj.image = cachedImage;
                    });

                });
            }
            catch (Exception e)
            {
                Console.WriteLine("URL LOAD :" + e.StackTrace);
            }

        }

        static UIImage FromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                if (data != null)
                    return UIImage.LoadFromData(data);
            return null;
        }


        public override bool ShouldAutorotate()
        {
            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;
            if(orientation == UIDeviceOrientation.Portrait)
                return false;
            else
                return true;
        }

        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
        {
            return UIInterfaceOrientation.Portrait;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
        {
            return UIInterfaceOrientationMask.Portrait;
        }
    }
}