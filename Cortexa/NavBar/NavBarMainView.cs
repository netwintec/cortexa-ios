using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace Cortexa
{
    public partial class NavBarMainView : UINavigationBar
    {

        UIImageView centerlogo,back, Menu, separator;

        public NavBarMainView (IntPtr handle) : base (handle)
        {
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            try
            {
                back.RemoveFromSuperview();
                centerlogo.RemoveFromSuperview();
                Menu.RemoveFromSuperview();
                separator.RemoveFromSuperview();
            }
            catch (Exception e) { }

            //Console.WriteLine(Frame.Width);

            centerlogo = new UIImageView(new CGRect(this.Frame.Width / 2 - 12, 10, 24, 24));
            centerlogo.Image = UIImage.FromFile("LogoPiccolo.png");

            Menu = new UIImageView(new CGRect(10, 10, 24, 24));
            Menu.Image = UIImage.FromFile("MenuSpento.png");

            separator = new UIImageView(new CGRect(0, 36, this.Frame.Width, 8));
            separator.Image = UIImage.FromFile("NavShade.png");

            back = new UIImageView(new CGRect(this.Frame.Width - 34, 10, 24, 24));
            back.Image = UIImage.FromFile("Back.png");

            AddSubview(centerlogo);
            AddSubview(Menu);
            AddSubview(separator);
            AddSubview(back);
        }

    }
}