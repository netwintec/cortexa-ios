using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UIKit;
using Xamarin.SWRevealViewController;

namespace Cortexa
{
    public partial class VideoPageController : UIViewController
    {
        int intDisplayWidth;
        int intDisplayHeight;

        List<UIWebView> ListWebView = new List<UIWebView>();
        List<string> ListUrl = new List<string>();

        float W, H, WApp, HApp;
        public bool change = false;

        public static VideoPageController Instance { private set; get; }

        public VideoPageController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            VideoPageController.Instance = this;

            int PageGiusta = 5;
            for (int i = 0; i < 12; i++)
            {
                if (i == PageGiusta)
                    AppDelegate.Instance.FlagPage[i] = true;
                else
                    AppDelegate.Instance.FlagPage[i] = false;
            }

            UIDeviceOrientation orientation = UIDevice.CurrentDevice.Orientation;

            if (!change)
            {
                if (orientation == UIDeviceOrientation.FaceUp || orientation == UIDeviceOrientation.FaceDown)
                {
                    if (View.Frame.Width < View.Frame.Height)
                        orientation = UIDeviceOrientation.Portrait;
                    else
                        orientation = UIDeviceOrientation.LandscapeLeft;
                }

                if (orientation == UIDeviceOrientation.Portrait || orientation == UIDeviceOrientation.PortraitUpsideDown)
                {
                    WApp = (float)View.Frame.Width;
                    HApp = (float)View.Frame.Height;

                    W = WApp;
                    H = HApp - 64;
                }
                if (orientation == UIDeviceOrientation.LandscapeRight || orientation == UIDeviceOrientation.LandscapeLeft)
                {
                    HApp = (float)View.Frame.Width;
                    WApp = (float)View.Frame.Height;

                    W = HApp;
                    H = WApp - 64;
                }


            }
            else
            {
                if (orientation == UIDeviceOrientation.Portrait)
                {
                    W = WApp;
                    H = HApp - 64;
                }
                else
                {
                    W = HApp;
                    H = WApp - 64;
                }
            }

            ContentView.Frame = new CGRect(0, 64, W, H);

            ListUrl.Clear();
            ListWebView.Clear();

            intDisplayWidth = (int)(ContentView.Frame.Width-20);
             intDisplayHeight = (int)((intDisplayWidth / 16) * 9);

             UIScrollView sv = new UIScrollView(new CGRect(0,0,ContentView.Frame.Width,ContentView.Frame.Height));

             int yy = 0;
             foreach (VideoObject obj in AppDelegate.Instance.ListVideo)
             {

                 UIWebView web = new UIWebView(new CGRect(10, yy+10, ContentView.Frame.Width-20, intDisplayHeight));
                 web.ScrollView.Bounces = false;
                 web.BackgroundColor = UIColor.White;
                 web.AllowsInlineMediaPlayback = true;

                 yy += (int)(10 + intDisplayHeight);

                UILabel lab = new UILabel(new CGRect(10, yy, ContentView.Frame.Width - 20, 20));
                lab.Text = obj.titolo;
                lab.TextColor = UIColor.Black;
                lab.Font = UIFont.FromName("OpenSans", 16);
                lab.BackgroundColor = UIColor.White;

                yy += 20;

                sv.Add(web);
                sv.Add(lab);

                ListWebView.Add(web);
                ListUrl.Add(obj.url);

             }

             sv.ContentSize = new CGSize(ContentView.Frame.Width, yy + 10);

             ContentView.Add(sv);

             FnPlayInWebView();


            if (this.RevealViewController() == null)
                return;
            this.RevealViewController().RightViewRevealOverdraw = 0.0f;
            MenuItem.Clicked += MenuItem_Clicked;
            View.AddGestureRecognizer(this.RevealViewController().PanGestureRecognizer);

            BackIcon.Clicked += BackIcon_Clicked;
        }

        //******** YOUTUBE FUNCTION ***************
        void FnPlayInWebView()
        {


            for (int i = 0; i < ListWebView.Count; i++)
            {
                string strUrl = ListUrl[i];

                string id = FnGetVideoID(strUrl);

                if (!string.IsNullOrEmpty(id))
                {
                    strUrl = string.Format("http://youtube.com/embed/" + id);
                }
                else
                {
                    Console.WriteLine("Video url is not in correct format");
                    return;
                }

                string html = @"<html><body style=""margin:0!important;padding:0!important""><iframe webkit-playsinline width=""videoWidth"" height=""videoHeight"" src=""strUrl?feature=player_detailpage&playsinline=1"" frameborder = ""0""></iframe></body></html>";
                UIWebView myWebView = ListWebView[i];
                string strYouTubeURL = html.Replace("videoWidth", intDisplayWidth.ToString()).Replace("videoHeight", intDisplayHeight.ToString()).Replace("strUrl", strUrl);

                myWebView.LoadHtmlString(strYouTubeURL, null);
            }
        }


        static string FnGetVideoID(string strVideoURL)
        {
            const string regExpPattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
            //for Vimeo: vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)
            var regEx = new Regex(regExpPattern);
            var match = regEx.Match(strVideoURL);
            return match.Success ? match.Groups[1].Value : null;
        }


        /************** END YOUTUBE FUNCTION *********/

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
            var segueReveal = segue as SWRevealViewControllerSegueSetController;
            if (segueReveal == null)
            {
                return;
            }
            this.RevealViewController().PushFrontViewController(segueReveal.DestinationViewController, true);
        }

        public override bool PrefersStatusBarHidden()
        {
            return false;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            foreach (var b in ContentView.Subviews)
                b.RemoveFromSuperview();

            MenuItem.Clicked -= MenuItem_Clicked;
            BackIcon.Clicked -= BackIcon_Clicked;

            change = true;

            base.WillRotate(toInterfaceOrientation, 0);
            this.ViewDidLoad();
        }

        private void BackIcon_Clicked(object sender, EventArgs e)
        {
            UIStoryboard storyboard = new UIStoryboard();
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
            {
                storyboard = UIStoryboard.FromName("Main", null);
            }
            else
            {
                storyboard = UIStoryboard.FromName("Main_iPad", null);
            }
            UIViewController HP = storyboard.InstantiateViewController("Home");
            this.RevealViewController().PushFrontViewController(HP, true);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            this.RevealViewController().RevealToggleAnimated(true);
        }

        public static Task<int> ShowAlert(string title, string message, params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }

        public async void Notification(string type)
        {
            string msg = "";
            if (type == "sondaggio")
                msg = "Nuovo sondaggio disponibile";
            if (type == "news")
                msg = "Nuova news disponibile";
            if (type == "manuale")
                msg = "Manuale aggiornato controllalo subito";
            if (type == "corso")
                msg = "Nuovo corso disponibile";
            if (type == "video")
                msg = "Nuovo video disponibile";

            UIStoryboard storyboard;
            int button = await ShowAlert("Notifica", msg + "\nVisualizzare?", "Si", "No");
            Console.WriteLine("Click" + button);
            if (button == 0)
            {

                InvokeOnMainThread(() =>
                {

                    UIStoryboard storyboard2 = new UIStoryboard();
                    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone)
                    {
                        storyboard2 = UIStoryboard.FromName("Main", null);
                    }
                    else
                    {
                        storyboard2 = UIStoryboard.FromName("Main_iPad", null);
                    }

                    UIViewController NP = storyboard2.InstantiateViewController("News");
                    UIViewController VP = storyboard2.InstantiateViewController("Video");
                    UIViewController WP = storyboard2.InstantiateViewController("Webinar");
                    UIViewController MP = storyboard2.InstantiateViewController("Manuale");
                    UIViewController SHP = storyboard2.InstantiateViewController("SondaggioHome");

                    if (type == "sondaggio")
                        this.RevealViewController().PushFrontViewController(SHP, true);
                    if (type == "news")
                        this.RevealViewController().PushFrontViewController(NP, true);
                    if (type == "manuale")
                        this.RevealViewController().PushFrontViewController(MP, true);
                    if (type == "corso")
                        this.RevealViewController().PushFrontViewController(WP, true);
                    if (type == "video")
                        this.RevealViewController().PushFrontViewController(VP, true);
                });
            }
            else { }
        }

    }
}