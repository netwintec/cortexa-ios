// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Cortexa
{
    [Register ("TableCell")]
    partial class TableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label0 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label5 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label6 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label7 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label8 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator0 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator3 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator4 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator5 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator6 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator7 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView Separator8 { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Label0 != null) {
                Label0.Dispose ();
                Label0 = null;
            }

            if (Label1 != null) {
                Label1.Dispose ();
                Label1 = null;
            }

            if (Label2 != null) {
                Label2.Dispose ();
                Label2 = null;
            }

            if (Label3 != null) {
                Label3.Dispose ();
                Label3 = null;
            }

            if (Label4 != null) {
                Label4.Dispose ();
                Label4 = null;
            }

            if (Label5 != null) {
                Label5.Dispose ();
                Label5 = null;
            }

            if (Label6 != null) {
                Label6.Dispose ();
                Label6 = null;
            }

            if (Label7 != null) {
                Label7.Dispose ();
                Label7 = null;
            }

            if (Label8 != null) {
                Label8.Dispose ();
                Label8 = null;
            }

            if (Separator0 != null) {
                Separator0.Dispose ();
                Separator0 = null;
            }

            if (Separator1 != null) {
                Separator1.Dispose ();
                Separator1 = null;
            }

            if (Separator2 != null) {
                Separator2.Dispose ();
                Separator2 = null;
            }

            if (Separator3 != null) {
                Separator3.Dispose ();
                Separator3 = null;
            }

            if (Separator4 != null) {
                Separator4.Dispose ();
                Separator4 = null;
            }

            if (Separator5 != null) {
                Separator5.Dispose ();
                Separator5 = null;
            }

            if (Separator6 != null) {
                Separator6.Dispose ();
                Separator6 = null;
            }

            if (Separator7 != null) {
                Separator7.Dispose ();
                Separator7 = null;
            }

            if (Separator8 != null) {
                Separator8.Dispose ();
                Separator8 = null;
            }
        }
    }
}