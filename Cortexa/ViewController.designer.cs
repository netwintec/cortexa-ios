﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Cortexa
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ButtonCorner { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView Load { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView LoadView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Login { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Password { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Registrati { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Utente { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ButtonCorner != null) {
                ButtonCorner.Dispose ();
                ButtonCorner = null;
            }

            if (Load != null) {
                Load.Dispose ();
                Load = null;
            }

            if (LoadView != null) {
                LoadView.Dispose ();
                LoadView = null;
            }

            if (Login != null) {
                Login.Dispose ();
                Login = null;
            }

            if (Password != null) {
                Password.Dispose ();
                Password = null;
            }

            if (Registrati != null) {
                Registrati.Dispose ();
                Registrati = null;
            }

            if (Utente != null) {
                Utente.Dispose ();
                Utente = null;
            }
        }
    }
}